<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
<?php include('header.php');?>
            <div class="content-container" style="padding-top: 98px !important;">
                <div class="heading-container">
                    <div class="heading-standar">
                        <div class="heading-wrap">
                            <div class="container">
                                <div class="page-title">
                                    <h1>Services</h1>
                                </div>
                            </div>
                            <div class="page-breadcrumb">
                                <div class="container">
                                    <ul class="breadcrumb">
                                        <li><a class="home" href="#"><span>Home</span></a></li>
                                        <li>Services</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="section-about">
                                    <div class="container">
                                        <div class="row" id = "Reiki">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <div class="empty-space-40"></div>
                                                    <img width="554" height="378" src="images/about-image.jpg" alt="about-image"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Reiki</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Reiki, literally translated from Japanese means ‘Universal Life Force Energy’ or ‘Universal Energy – Rei meaning Universal and Ki being energy. From the traditional teachings of Dr. Usui (the father of Reiki), another perspective is that Reiki also means “Spiritual Energy, our own innate brightness, our true nature, our basic essence.
                                                    </p>
                                                     <p> Reiki is an energy therapy that works with the body’s own remarkable healing capabilities. It helps to promote and accelerate the natural healing process and restore balance on all levels; physical, mental and emotional.  It is energy at its purest. </p>
                                                     <p> Some of their benefits include:</p>
                                                </div>
                                                <div class="testimonial testimonial-style-center">
                                                    <div class="caroufredsel" data-visible="1" data-scroll-fx="scroll" data-speed="5000" data-responsive="1" data-infinite="1" data-autoplay="0">
                                                        <div class="caroufredsel-wrap">
                                                            <ul class="caroufredsel-items">
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                            Stress and pain relief
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                            Strengthen the immune system
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                             Promote personal awareness
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                             Clears toxins
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                             Clears emotional blockages
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                             Balances chakras
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                              Promote relaxation and sleep
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                             First –aid
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="caroufredsel-item">
                                                                    <div class="testimonial-wrap">
                                                                        <div class="testimonial-text">
                                                                            <span>&ldquo;</span>
                                                                             Improve intuition
                                                                            <span>&rdquo;</span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="caroufredsel-pagination"></div>
                                                    </div>
                                                </div>
                                                <div class="accordion">
                                                    <div id="accordion-1" class="panel-group">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading panel-icon-none-right">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-1">Usui Reiki Beginners (Level 1</a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion-1-1" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="text-block">
                                                                        <p>
                                                                            Usui Reiki Level 1 Beginners Workshop is a fantastic foundation course for those interested in self healing and personal empowerment, that gives you Reiki for life. Many people who are using Reiki for themselves and for their friends and family find this level fulfilling to manage stress, enhance calm and peace as well as for first aid.
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading panel-icon-none-right">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-2">Usui Reiki Advanced (Level 2)</a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion-1-2" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="text-block">
                                                                        <p>
                                                                            Usui Reiki Level 2 is taken at least 21 days of regular practice after Level 1.This period of time is an important opportunity to practice Reiki and soak up its benefits for self-healing and understanding energy. During the Level 2 Workshop, a second degree attunement is given and many symbols are taught, to enhance the Reiki flow, and to develop additional uses of the Reiki energy. 
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading panel-icon-none-right">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-3">Usui Reiki Masters (Level 3)</a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion-1-3" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="text-block">
                                                                        <p>
                                                                            We often describe Reiki Level 3 as the most shifting class you can take, the “mother of all workshops”! The Master’s symbol is also known as the symbol for transformation and is a very powerful tool for personal growth and can be extremely challenging for those who have not mastered the Level 2 techniques and/or have a daily self healing practice in place.  Most people experience huge shifts of conscious afte the class. 
                                                                            <p>
                                                                            As well as preparing you for the rest of the Master training, this module works on raising your energy vibration, understanding your own power through developing your intuition, as well as becoming familiar with the attunement process and how to best utilise the Master’s symbol.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                                                      
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Teachings of Pranashakty">
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Teachings of Pranashakty</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Pranashakty international, an international spiritual organization which promotes sadhanas for enhancing Physical, Mental, Emotional & Spiritual Wellbeing.
                                                    </p>
                                                </div>
                                                <div class="border_align_left"> <button data-toggle="modal" data-target="#sss" class="no_button" type="button">
                                                                    <span>Sarva Shakti Sadhana (SSS)</span> </button> </div> 
                                                <div class="border_align_left"> <button data-toggle="modal" data-target="#ananda" class="no_button" type="button">
                                                                    <span>Ananda Siddhi Diksha</span> </button> </div>
                                                <div class="border_align_left"> <button data-toggle="modal" data-target="#helium" class="no_button" type="button">
                                                                    <span>Helium Activation</span> </button> </div>
                                                <div class="border_align_left"> <button data-toggle="modal" data-target="#neuro" class="no_button" type="button">
                                                                    <span>Neuro Cell Rebirth Process</span> </button> </div>
                                                <div class="border_align_left"> <button data-toggle="modal" data-target="#kundalini" class="no_button" type="button">
                                                                    <span>Kundalini Shakthipat</span> </button> </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/pranashakthy.jpg" alt="pranashakthy"/>
                                                </div>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-6">
                                                <div class="modal fade" id="sss" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    <span class="sr-only">Close</span>
                                                                </button>
                                                                <h4 class="modal-title">Sarva Shakti Sadhana</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Sarva Shakti Sadhana is powerful breathing exercise combine with body movements which will fill you with high vibrations of prana. The practice needs a special diksha. It will deeply cleanse and activate your chakras and will help you achieve the pranamaya kosha.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="ananda" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    <span class="sr-only">Close</span>
                                                                </button>
                                                                <h4 class="modal-title">Ananda Siddhi Diksha</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Ananda means BLISS. Ananda Siddhi meditation fills with instant bliss and healing to practitioners. They are given a mantra and just chanting the mantra the mind and body will be released of stress and strain. This energy can be used for healing themselves and others. Ananda Siddhi energy is excellent for healing mental and emotional issues. Some of the effects of this diksha are: </p>
                                                                    <ul>
                                                                        <li type="square">In some cases, instant healing will happen - especially for emotional / mental disorders.</li>
                                                                        <li type="square">It also induces a thoughtless meditation and Thuriya state is achieved easily and without effort.</li>
                                                                        <li type="square">It creates inner peace & leads to greater compassion.</li>
                                                                        <li type="square">It lowers Stress & Anxiety.</li>
                                                                        <li type="square">It makes lots of improvement in all relationships.</li>
                                                                        <li type="square">It lowers blood pressure & enhances sleep patterns.</li>
                                                                        <li type="square">It will awaken your intuition.</li>
                                                                        <li type="square">Distant healing possible.</li>
                                                                        <li type="square">Healing can be given to anyone, anywhere in the world.</li>
                                                                    </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="helium" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    <span class="sr-only">Close</span>
                                                                </button>
                                                                <h4 class="modal-title">Helium Activation</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Helium energy is rare among the rarest energies. This has the ability to transform your DNA completely and awaken the complete potential of a human being. After getting initiated it automatically grows in you without any practice. It slowly transforms and cleanses you. This activation will change your body’s DNA and activate the inherent intelligent system. It will help to heal the organs inside your body, improve the blood flow & cleanse your nadis / meridian
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="neuro" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    <span class="sr-only">Close</span>
                                                                </button>
                                                                <h4 class="modal-title">Neuro Cell Rebirth Process</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    NCRP is a profound healing therapy taking place on the molecular level of each & every cell of the body, allowing for a deep emotional release at the cellular level which triggers a powerful transformation and regenerate process affecting body mind and soul. NCRP works to cleanse all samskaras or deeps impressions and karma – inducing agents rooted in the mind, from the patterned makeup of each cell. NCRP is a group meditational healing method or therapy that solves all past, present, and future emotional problems.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="kundalini" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    <span class="sr-only">Close</span>
                                                                </button>
                                                                <h4 class="modal-title">Kundalini Shakthipat </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Kundalini is the source from which all human life has formed and is the force within us through which all spiritual experiences are derived. Pranashakty offers Kundalini Shakthipat for Spiritual Aspirants. After the Awakening, The Aspirant is never the same. He / She gets access to many aspects of reality and his/her true nature. Since then, spiritual development happens automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="empty-space-40"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Hypnotherapy">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/hypnotherapy.jpg" alt="hypnotherapy"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Hypnotherapy</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Hypnotherapy is a form of therapy used to reprogram the subconscious mind. When under hypnosis, you put your mind and body into a heightened state of learning, making you more susceptible to suggestions for self-improvement or behavior modification. The goal is to put the subconscious and conscious mind in harmony, which in turn helps give you greater control over your behavior and emotions.
                                                    </p>
                                                     <p> Hypnotherapy is a wonderful tool to cure any psychosomatic,psychological,Mental and Emotional problems.It can clear any phobias,fears,Blocks,Traumas, Personality disorders, Relationship issues etc</p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div> 
                                        
                                        <div class="row" id = "NLP">
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>NLP</h2>
                                                <div class="text-block">
                                                    <p>
                                                        NLP stands for Neuro-Linguistic Programming. Neuro refers to your neurology; Linguistic refers to language; programming refers to how that neural language functions. In other words, learning NLP is like learning the language of your own mind!
                                                        NLP is a Unique Model of how people learn, motivate themselves and change their behaviour TO ACHIEVE EXCELLENCE
                                                        <p>
                                                        NLP can treat problems such as phobias, depression, tic disorders, psychosomatic illnesses, near-sightedness, allergy, common cold, and learning disorders.
                                                        the key benefits of NLP are: 

                                                    </p>
                                                    
                                                    <div class="testimonial testimonial-style-center">
                                                        <div class="caroufredsel" data-visible="1" data-scroll-fx="scroll" data-speed="5000" data-responsive="1" data-infinite="1" data-autoplay="0">
                                                            <div class="caroufredsel-wrap">
                                                                <ul class="caroufredsel-items">
                                                                    <li class="caroufredsel-item">
                                                                        <div class="testimonial-wrap">
                                                                            <div class="testimonial-text">
                                                                                <span>&ldquo;</span>
                                                                                Improve communication with yourself and others.
                                                                                <span>&rdquo;</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <div class="testimonial-wrap">
                                                                            <div class="testimonial-text">
                                                                                <span>&ldquo;</span>
                                                                                Change behaviours, thinking and beliefs.
                                                                                <span>&rdquo;</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <div class="testimonial-wrap">
                                                                            <div class="testimonial-text">
                                                                                <span>&ldquo;</span>
                                                                                Replicate excellence.
                                                                                <span>&rdquo;</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="caroufredsel-pagination"></div>
                                                        </div>
                                                    </div>
                                                    <blockquote>
                                                        <p>
                                                          NLP Techniques
                                                        </p>
                                                    </blockquote>
                                                    <p>NLP techniques are designed to be supported by important subtle skills including rapport, calibration, language patterns, anchoring, and belief preparation, Change of Habit, removal of phobias, fear etc, Internal Representation, timeline therapy and many more etc</p>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/nlp.jpg" alt="nlp"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "BACHFLOWER">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/bachflower.jpg" alt="bachflower"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>BACHFLOWER Remedies</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Bach Flower Remedies (BFRs) are a widely-available, popular form of Complementary and Alternative Medicine (CAM) developed in the 1930s by the British physician Dr Edward Bach. Bach devoted his life to the discovery of 38 remedies that correspond to 38 negative emotional states.  Bach believed in a truly holistic form of emotional healing; BFRs are believed to assist the body in healing itself by providing "a positive emotional state that is conducive to the restoration of a healthy equilibrium and by acting to catalyze an individual's own internal resources for maintaining balance".
                                                    </p>
                                                     <p> According to Bach, the restoration of balance could be used for treating any medical condition.</p>
                                                </div>
                                            </div>
                                        </div>
                                  
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "BAT">
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>BAT</h2>
                                                <div class="text-block">
                                                    <p>
                                                        BAT or Bone Alignment therapy as the name suggest is about alignment of our bodily  structures.Most of our bodily pains are because of bad postures and misalignment of the bones .It's a very effective Therapy to cure such structural long lasting issues causing pain.
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/bat.jpg" alt="bat"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Reflexology">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/reflexology.jpg" alt="reflexology"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Reflexology & Sujok</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Reflexology is the application of appropriate pressure to specific points and areas on the feet, hands, or ears. Reflexologists believe that these reflex points correspond to different body organs and systems, and that pressing them creates real benefits for the person's health.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Mahalakshmi Diksha">
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Mahalakshmi Diksha</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Receive the divine prosperity energy from The Mother Mahalekshmi through Sri Kannan jothi- Who is empowered by Pranasiddar Manaimaran of  Pranashakty and The Mother Goddess Lekshmi – and with true intent and devotion, manifest your dreams of health, wealth, prosperity and beauty. 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/mahalakshmi-diksha.jpg" alt="mahalakshmi-diksha"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Memory Empowerment">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/memory-empowerment.jpg" alt="memory-empowerment"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Memory Empowerment</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Memory Empowerment program is aiming at improving the memory and makes children sharp through mid-brain activation. This includes giving a diksha and chanting a mantra to get through the awakening.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Yoga">
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Yoga</h2>
                                                <div class="text-block">
                                                    <p> To perform every action artfully is yoga </p>
                                                    <p>
                                                        Join hands to walk through and realize the real ‘YOU’. Starting from Pranayamas through basic Asanas to complete Yogasanas. You will slowly experience the inner YOU. Yoga is not merely an exercise it’s a “WAY OF LIFE” . Come explore it and feel the real happiness within and blissfull being. 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/about-image.jpg" alt="about-image"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="empty-space-40"></div>
                                        
                                        <div class="row" id = "Hoponopono">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/hypnotherapy.jpg" alt="hypnotherapy"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>Ho'oponopono</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Learn all about the Hawaiian word for “forgiveness”. In Hawaii, Ho’oponopono means: “I’m sorry, please forgive me, Thank you , I Love you.”  Ho’opnopono is not only a way of healing ourselves, but others and our world as well.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                            
                                        <div class="empty-space-40"></div>
                                        
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php include('footer.php'); ?>

        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>
        
        <script type='text/javascript' src='js/libs/jquery.js'></script>
        <script type='text/javascript' src='js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='js/libs/easing.min.js'></script>
        <script type='text/javascript' src='js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.parallax.js'></script>

        <script type='text/javascript' src='js/skin-selector.js'></script>

        <script type='text/javascript' src='js/script.js'></script>

    </body>
</html>