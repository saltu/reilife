
    <header class="header-container header-type-default header-default-center header-navbar-default header-absolute header-transparent">
        <div class="navbar-container header-colour">
            <div class="navbar navbar-default navbar-scroll-fixed">
                <div class="navbar-default-wrap ">
                    <div class="container">
                        <div class="navbar-wrap">
                            <div class="navbar-header">
                                <button data-target=".primary-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar bar-top"></span>
                                    <span class="icon-bar bar-middle"></span>
                                    <span class="icon-bar bar-bottom"></span>
                                </button>
                                <a class="cart-icon-mobile" href="#"><i class="elegant_icon_bag"></i> <span>0</span></a>
                                <a class="navbar-brand" title="Reilife" href="index.php">
                                    <img class="logo" alt="Reilife" src="images/logo-white.png">
                                    <img class="logo-fixed" alt="Reilife" src="images/logo-fixed.png">
                                    <img class="logo-mobile" alt="Reilife" src="images/logo-mobile.png">
                                </a>
                            </div>
                            <nav class="collapse navbar-collapse primary-navbar-collapse">
                                <ul class="nav navbar-nav primary-nav">
                                    <li class="menu-item-has-children dropdown">
                                        <a title="Home" href="index.php" class="dropdown-hover">
                                            <span class="underline">Home</span> <span class="caret"></span>
                                        </a>
                                    </li>
                                    <li class="menu-item-has-children megamenu dropdown">
                                        <a title="Services" href="services.php" class="dropdown-hover">
                                            <span class="underline">Services</span> <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">

                                            <li class="menu-item-has-children mega-col-4 dropdown">
                                                <ul class="dropdown-menu">
                                                    <li><a title="Reiki" href="services.php#Reiki">Reiki</a></li>
                                                    <li><a title="TOFPI" href="services.php#Teachings of Pranashakty">Teachings of Pranashakty International</a></li>
                                                    <li><a title="NLP" href="services.php#NLP">NLP</a></li>
                                                    <li><a title="HypnoTherapy" href="services.php#Hypnotherapy"> HypnoTherapy</a></li>
                                                    <li><a title="EFT" href="services.php">EFT</a></li>
                                                    <li><a title="BACHFLOWER" href="services.php#BACHFLOWER">Bachflower Remedies</a></li>
                                                    <li><a title="BAT" href="services.php#BAT">BAT(Bone Alignment Therapy)</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children mega-col-4 dropdown">
                                                <ul class="dropdown-menu">
                                                    
                                                    <li><a title="Reflexology" href="services.php#Reflexology">Reflexology & Sujok</a></li>
                                                    <li><a title="Mahalakshmi Diksha" href="services.php#Mahalakshmi Diksha">Mahalakshmi Diksha</a></li>
                                                    <li><a title="Memory EMpowerment" href="services.php#Memory Empowerment">Memory Empowerment</a></li>
                                                    <li><a title="Yoga" href="services.php#Yoga">Yoga</a></li>
                                                    <li><a title="HOPONOPONO" href="services.php#HOPONOPONO">Ho'ponopono</a></li>
                                                    <li><a title="DOWSING" href="services.php#DOWSING">Dowsing</a></li>
                                                    <li><a title="martialarts" href="services.php">Martial Arts</a></li>
                                                </ul>
                                            </li>
                                            <li class="visible-lg-inline mega-col-4"></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item-has-children dropdown">
                                        <!-- <a title="Blog" href="blog.php" class="dropdown-hover">
                                            <span class="underline">Blog</span> <span class="caret"></span>
                                        </a> -->
                                    </li>
                                    <li class="menu-item-navbar-brand">
                                        <a class="navbar-brand" href="index.php">
                                            <img class="logo" alt="Reilife" src="images/logo-white.png">
                                            <img class="logo-fixed" alt="Reilife" src="images/logo-fixed.png">
                                            <img class="logo-mobile" alt="Reilife" src="images/logo-mobile.png">
                                        </a>
                                    </li>
                                    <li class="menu-item-has-children dropdown">
                                       <!--  <a title="Events" href="event.php" class="dropdown-hover">
                                            <span class="underline">Events</span> <span class="caret"></span>
                                        </a> -->
                                    </li>
                                    <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown bg_2">
                                        <a title="About-Us" href="about-us.php" class="dropdown-hover">
                                            <span class="underline">About Us</span> <span class="caret"></span>
                                        </a>
                                    </li>
                                    
                                    <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown">
                                        <a title="Contact" href="contact.php" class="dropdown-hover">
                                            <span class="underline">Contact Us</span> <span class="caret"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
            