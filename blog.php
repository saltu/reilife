<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">

            <?php include('header.php');?>    
            <div class="content-container" style="padding-top: 98px !important;">
                <div class="heading-container">
                    <div class="heading-standar">
                        <div class="heading-wrap">
                            <div class="container">
                                <div class="page-title">
                                    <h1>Blog</h1>
                                </div>
                            </div>
                            <div class="page-breadcrumb">
                                <div class="container">
                                    <ul class="breadcrumb">
                                        <li><a class="home" href="#"><span>Home</span></a></li>
                                        <li>Blog</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-container"  style="padding-top:0px !important;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 main-wrap" style="overflow-y: scroll; height:1050px; ">
                            <div class="posts">
                                <div class="posts-wrap posts-layout-timeline">
                                    <div class="timeline-date">
                                        <span class="timeline-date-title">Apr 2015</span>
                                    </div>
                                    <article class="hentry timeline-align-left">
                                        <div class="timeline-badge">
                                            <a>
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span>April 4, 2015 3:06 pm</span>
                                            </a>
                                        </div>
                                        <div class="timeline-arrow"></div>
                                        <div class="hentry-wrap">
                                            <div class="entry-featured ">
                                                <a href="#">
                                                    <img width="700" height="350" src="images/blog/blog7.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="entry-info">
                                                <div class="entry-header">
                                                    <h2 class="entry-title">
                                                        <a href="#">Finibus Bonorum et Malorum</a>
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <span class="meta-author">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#">sitesao</a>
                                                        </span>
                                                        <span class="meta-category">
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <a href="#">Popular</a>, <a href="#">Trends</a>
                                                        </span>
                                                        <span class="meta-comment">
                                                            <i class="fa fa-comment-o"></i>
                                                            <a href="#" class="meta-comments">0 Comment</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia ex vel rutrum. Nam convallis molestie turpis eget pretium. Sed dignissim quam at egestas iaculis. Nulla facilisi. Nunc non felis ac turpis dictum consectetur. Suspendisse sit amet erat et tellus porttitor tincidunt in sed purus....
                                                    </p>
                                                    <p class="readmore-link text-right">
                                                        <span class="date">
                                                            <time datetime="2015-04-04T15:06:28+00:00">April 4, 2015</time>
                                                        </span>
                                                        <a href="#">Read More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="hentry timeline-align-right">
                                        <div class="timeline-badge">
                                            <a>
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span>April 3, 2015 3:04 pm</span>
                                            </a>
                                        </div>
                                        <div class="timeline-arrow"></div>
                                        <div class="hentry-wrap">
                                            <div class="entry-featured ">
                                                <a href="#">
                                                    <img width="700" height="350" src="images/blog/blog8.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="entry-info">
                                                <div class="entry-header">
                                                    <h2 class="entry-title">
                                                        <a href="#">Cras dapibus eget ligula</a>
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <span class="meta-author">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#">sitesao</a>
                                                        </span>
                                                        <span class="meta-category">
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <a href="#">Popular</a>, <a href="#">Trends</a>
                                                        </span>
                                                        <span class="meta-comment">
                                                            <i class="fa fa-comment-o"></i>
                                                            <a href="#" class="meta-comments">0 Comment</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia ex vel rutrum. Nam convallis molestie turpis eget pretium. Sed dignissim quam at egestas iaculis. Nulla facilisi. Nunc non felis ac turpis dictum consectetur. Suspendisse sit amet erat et tellus porttitor tincidunt in sed purus....
                                                    </p>
                                                    <p class="readmore-link text-right">
                                                        <span class="date">
                                                            <time datetime="2015-04-04T15:06:28+00:00">April 3, 2015</time>
                                                        </span>
                                                        <a href="#">Read More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <div class="timeline-date">
                                        <span class="timeline-date-title">Mar 2015</span>
                                    </div>
                                    <article class="hentry timeline-align-left">
                                        <div class="timeline-badge">
                                            <a>
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span>March 28, 2015 3:03 pm</span>
                                            </a>
                                        </div>
                                        <div class="timeline-arrow"></div>
                                        <div class="hentry-wrap">
                                            <div class="entry-info">
                                                <div class="entry-header">
                                                    <h2 class="entry-title">
                                                        <a href="#">Sed dignissim quam</a>
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <span class="meta-author">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#">sitesao</a>
                                                        </span>
                                                        <span class="meta-category">
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <a href="#">Popular</a>, <a href="#">Trends</a>
                                                        </span>
                                                        <span class="meta-comment">
                                                            <i class="fa fa-comment-o"></i>
                                                            <a href="#" class="meta-comments">0 Comment</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia ex vel rutrum. Nam convallis molestie turpis eget pretium. Sed dignissim quam at egestas iaculis. Nulla facilisi. Nunc non felis ac turpis dictum consectetur. Suspendisse sit amet erat et tellus porttitor tincidunt in sed purus....
                                                    </p>
                                                    <p class="readmore-link text-right">
                                                        <span class="date">
                                                            <time datetime="2015-04-04T15:06:28+00:00">March 28, 2015</time>
                                                        </span>
                                                        <a href="#">Read More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="hentry timeline-align-right">
                                        <div class="timeline-badge">
                                            <a>
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span>March 25, 2015 3:01 pm</span>
                                            </a>
                                        </div>
                                        <div class="timeline-arrow"></div>
                                        <div class="hentry-wrap">
                                            <div class="entry-featured ">
                                                <a href="#">
                                                    <img width="700" height="350" src="images/blog/blog9.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="entry-info">
                                                <div class="entry-header">
                                                    <h2 class="entry-title">
                                                        <a href="#">Pellentesque quis metus</a>
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <span class="meta-author">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#">sitesao</a>
                                                        </span>
                                                        <span class="meta-category">
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <a href="#">Popular</a>, <a href="#">Trends</a>
                                                        </span>
                                                        <span class="meta-comment">
                                                            <i class="fa fa-comment-o"></i>
                                                            <a href="#" class="meta-comments">0 Comment</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia ex vel rutrum. Nam convallis molestie turpis eget pretium. Sed dignissim quam at egestas iaculis. Nulla facilisi. Nunc non felis ac turpis dictum consectetur. Suspendisse sit amet erat et tellus porttitor tincidunt in sed purus....
                                                    </p>
                                                    <p class="readmore-link text-right">
                                                        <span class="date">
                                                            <time datetime="2015-04-04T15:06:28+00:00">March 25, 2015</time>
                                                        </span>
                                                        <a href="#">Read More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="hentry timeline-align-left">
                                        <div class="timeline-badge">
                                            <a>
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span>March 20, 2015 3:00 pm</span>
                                            </a>
                                        </div>
                                        <div class="timeline-arrow"></div>
                                        <div class="hentry-wrap">
                                            <div class="entry-featured ">
                                                <a href="#">
                                                    <img width="700" height="350" src="images/blog/blog10.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="entry-info">
                                                <div class="entry-header">
                                                    <h2 class="entry-title">
                                                        <a href="#">Sed hendrerit turpis ut turpis</a>
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <span class="meta-author">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#">sitesao</a>
                                                        </span>
                                                        <span class="meta-category">
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <a href="#">Popular</a>, <a href="#">Trends</a>
                                                        </span>
                                                        <span class="meta-comment">
                                                            <i class="fa fa-comment-o"></i>
                                                            <a href="#" class="meta-comments">0 Comment</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia ex vel rutrum. Nam convallis molestie turpis eget pretium. Sed dignissim quam at egestas iaculis. Nulla facilisi. Nunc non felis ac turpis dictum consectetur. Suspendisse sit amet erat et tellus porttitor tincidunt in sed purus....
                                                    </p>
                                                    <p class="readmore-link text-right">
                                                        <span class="date">
                                                            <time datetime="2015-04-04T15:06:28+00:00">March 20, 2015</time>
                                                        </span>
                                                        <a href="#">Read More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="hentry timeline-align-right">
                                        <div class="timeline-badge">
                                            <a>
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span>March 18, 2015 2:58 pm</span>
                                            </a>
                                        </div>
                                        <div class="timeline-arrow"></div>
                                  <div class="hentry-wrap">
                                            <div class="entry-featured ">
                                                <a href="#">
                                                    <img width="700" height="350" src="images/blog/blog19.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="entry-info">
                                                <div class="entry-header">
                                                    <h2 class="entry-title">
                                                        <a href="#">Aenean : cursus magna facilisis</a>
                                                    </h2>
                                                    <div class="entry-meta">
                                                        <span class="meta-author">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            <a href="#">sitesao</a>
                                                        </span>
                                                        <span class="meta-category">
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <a href="#">Popular</a>, <a href="#">Trends</a>
                                                        </span>
                                                        <span class="meta-comment">
                                                            <i class="fa fa-comment-o"></i>
                                                            <a href="#" class="meta-comments">0 Comment</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia ex vel rutrum. Nam convallis molestie turpis eget pretium. Sed dignissim quam at egestas iaculis. Nulla facilisi. Nunc non felis ac turpis dictum consectetur. Suspendisse sit amet erat et tellus porttitor tincidunt in sed purus....
                                                    </p>
                                                    <p class="readmore-link text-right">
                                                        <span class="date">
                                                            <time datetime="2015-04-04T15:06:28+00:00">March 18, 2015</time>
                                                        </span>
                                                        <a href="#">Read More</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="loadmore-action">
                                    <div class="loadmore-loading">
                                        <div class="fade-loading"><i></i><i></i><i></i><i></i></div>
                                    </div>
                                    <button type="button" class="btn-loadmore">Load More</button>
                                </div>
                            </div>
                        </div>
                                    
                        <aside class="col-md-3 sidebar-wrap">
                            <div class="main-sidebar">
                                <div class="widget widget_search">
                                    <h4 class="widget-title"><span>Search</span></h4>
                                    <form>
                                        <input type="search" class="form-control" placeholder="Search"/>
                                        <input type="submit" class="hidden" name="submit" value="Search"/>
                                    </form>
                                </div>
                                <div class="widget widget-post-thumbnail">
                                    <h4 class="widget-title">
                                        <span>Latest Post</span>
                                    </h4>
                                    <ul class="posts-thumbnail-list">
                                        <li>
                                            <div class="posts-thumbnail-image">
                                                <a href="#">
                                                    <img width="60" height="50" src="images/blog/blog3.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="posts-thumbnail-content">
                                                <time datetime="2015-04-04T15:06:28+00:00">Apr 4, 2015</time>
                                                <span class="comment-count">
                                                    <a href="#"><i class="fa fa-comments-o"></i>0</a>
                                                </span>
                                                <h4>
                                                    <a href="#">Finibus Bonorum et Malorum</a>
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="posts-thumbnail-image">
                                                <a href="#">
                                                    <img width="60" height="50" src="images/blog/blog4.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="posts-thumbnail-content">
                                                <time datetime="2015-04-04T15:06:28+00:00">Apr 3, 2015</time>
                                                <span class="comment-count">
                                                    <a href="#"><i class="fa fa-comments-o"></i>0</a>
                                                </span>
                                                <h4>
                                                    <a href="#">Cras dapibus eget ligula</a>
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="posts-thumbnail-image">
                                                <a href="#">
                                                    <img width="600" height="450" src="images/blog/blog5.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="posts-thumbnail-content">
                                                <time datetime="2015-04-04T15:06:28+00:00">Mar 25, 2015</time>
                                                <span class="comment-count">
                                                    <a href="#"><i class="fa fa-comments-o"></i>0</a>
                                                </span>
                                                <h4>
                                                    <a href="#">Pellentesque quis metus</a>
                                                </h4>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="posts-thumbnail-image">
                                                <a href="#">
                                                    <img width="600" height="450" src="images/blog/blog6.jpg" alt="blog" />
                                                </a>
                                            </div>
                                            <div class="posts-thumbnail-content">
                                                <time datetime="2015-04-04T15:06:28+00:00">Mar 20, 2015</time>
                                                <span class="comment-count">
                                                    <a href="#"><i class="fa fa-comments-o"></i>0</a>
                                                </span>
                                                <h4>
                                                    <a href="#">Sed hendrerit turpis ut turpis</a>
                                                </h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget tweets-widget">
                                    <h4 class="widget-title">
                                        <span>Popular Tweets</span>
                                    </h4>
                                    <div class="recent-tweets">
                                        <ul>
                                            <li>
                                                <span>RT<a href="#" title="Follow EnvatoMarket" target="_blank">@EnvatoMarket</a>: The Best &amp; Worst Web Design Trends of 2014 <a href="#" target="_blank">http://t.co/CwQkBeTsUx</a> <a href="#" target="_blank">http://t.co/LZW0aGzKtV</a></span>
                                                <a class="twitter_time" target="_blank" href="#">226 days ago</a>
                                            </li>
                                            <li>
                                                <span>RT <a href="#" title="Follow envato" target="_blank">@envato</a>: "Good design doesn't date. Bad design does." Paul Rand.</span>
                                                <a class="twitter_time" target="_blank" href="#">226 days ago</a>
                                            </li>
                                            <li>
                                                <span>RT <a href="#" target="_blank">@envato</a>: Thinking about an exit strategy in early days of a startup, advice given to <a href="#" target="_blank">@collis</a> <a href="#" target="_blank">http://t.co/DOHxmOVlDx</a></span>
                                                <a class="twitter_time" target="_blank" href="#">290 days ago</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="widget widget_tag_cloud">
                                    <h4 class="widget-title"><span>Tags</span></h4>
                                    <div class="tagcloud">
                                        <a href='#'>Blog</a>
                                        <a href='#'>Fashion</a>
                                        <a href='#'>Shop</a>
                                        <a href='#'>Trends</a>
                                    </div>
                                </div>
                            </div>
                        </aside>   
                    </div>
                 </div>
            </div>
        <?php include('footer.php'); ?>
        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>

        <script type='text/javascript' src='js/libs/jquery.js'></script>
        <script type='text/javascript' src='js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='js/libs/easing.min.js'></script>
        <script type='text/javascript' src='js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.parallax.js'></script>

        <script type='text/javascript' src='js/skin-selector.js'></script>

        <script type='text/javascript' src='js/script.js'></script>

    </body>
</html>