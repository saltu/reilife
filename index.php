<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
        <?php 
        include('header.php');
        include('dbconnect.php');
        ?>
            <div class="content-container no-padding">
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="row">
                                    <div class="column col-md-12">
                                        <div id="revslider" data-autorun="yes" data-duration="10000" class="carousel slide fade dhslider dhslider-custom " data-height="590">
                                            <div class="dhslider-loader">
                                                <div class="fade-loading">
                                                    <i></i><i></i><i></i><i></i>
                                                </div>
                                            </div>
                                            <div class="carousel-inner dhslider-wrap">
                                                <div class="item slider-item active">
                                                    <div class="slide-bg slide-bg-1"></div>
                                                    <div class="slider-overlay"></div>
                                                    <div class="slider-caption caption-align-right">
                                                        <div class="slider-caption-wrap parallax-content">
                                                            <h2 class="slider-heading-text ">REILIFE</h2>
                                                            <div class="slider-caption-text">
                                                                An institute promoting Holistic healing and altunative therapies like Reiki, Acupressure, Sujok, EFT , Flower Remedies etc..
                                                            </div>
                                                            <div class="slider-buttons">
                                                                <a href="services.php" class="btn btn-default btn-success btn-style-solid">
                                                                    SEE MORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item slider-item">
                                                    <div class="slide-bg slide-bg-2"></div>
                                                    <div class="slider-overlay"></div>
                                                    <div class="slider-caption caption-align-left">
                                                        <div class="slider-caption-wrap parallax-content">
                                                            <h2 class="slider-heading-text">VISION OF REILIFE</h2>
                                                            <div class="slider-caption-text">
                                                                Restoring balance and harmony to mind, body and spirit.
                                                            </div>
                                                            <div class="slider-buttons">
                                                                <a href="about-us.php" class="btn btn-default btn-danger btn-style-solid">
                                                                    LEARN MORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ol class="carousel-indicators parallax-content">
                                                <li data-target="#revslider" data-slide-to="0" class="active"></li>
                                                <li data-target="#revslider" data-slide-to="1"></li>
                                            </ol>
                                            <a class="left carousel-control" href="#revslider" data-slide="prev">
                                                <i class="fa fa-angle-left carousel-icon-prev"></i>
                                            </a>
                                            <a class="right carousel-control" href="#revslider" data-slide="next">
                                                <i class="fa fa-angle-right carousel-icon-next"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row section-welcome section-background-bottom">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2 class="text-center">
                                                        WELCOME
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-2 col-sm-6"></div>
                                                <div class="column col-md-8 col-sm-8">
                                                    <div class="text-block">
                                                        <p class="text-center">
                                                            REILIFE, an institute promoting Holistic healing and altunative therapies for restoring balance and harmony to mind, body and spirit.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="column col-md-2 col-sm-6"></div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row welcome-items">
                                                <div class="column col-md-4 col-sm-4" data-fade="1" data-fade-animation="in">
                                                    <div class="single-image image-center">
                                                        <img width="500" height="250" src="images/services/service-1.jpg" class="box-default" alt="service-1"/>
                                                    </div>
                                                    <div class="box box-custom-margin-padding">
                                                        <h4>VISION</h4>
                                                        <div class="text-block text-center">
                                                            <p>
                                                                Ut ultricies quis nulla non lacinia. Suspendisse posuere orci enim, ac imperdiet diam mattis nec. Etiam ante nisi.
                                                            </p>
                                                            <p>
                                                                <a href="#">See More <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4" data-fade="1" data-fade-animation="in">
                                                    <div class="single-image image-center">
                                                        <img width="500" height="250" src="images/services/service-2.jpg" class="box-default" alt="service-2"/>
                                                    </div>
                                                    <div class="box box-custom-margin-padding">
                                                        <h4>MISSION</h4>
                                                        <div class="text-block text-center">
                                                            <p>
                                                                Quisque ullamcorper libero urna. Integer in lacus gravida, pharetra neque quis, dictum diam. Donec faucibus varius.
                                                            </p>
                                                            <p>
                                                                <a href="#">See More <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4" data-fade="1" data-fade-animation="in">
                                                    <div class="single-image image-center">
                                                        <img width="500" height="250" src="images/services/service-3.jpg" class="box-default" alt="service-3"/>
                                                    </div>
                                                    <div class="box box-custom-margin-padding">
                                                        <h4>VALUES</h4>
                                                        <div class="text-block text-center">
                                                            <p>
                                                                Nam id porttitor lacus. Fusce cursus porttitor pulvinar. Maecenas risus nunc, ornare et ultricies id, vestibulum in metus.
                                                            </p>
                                                            <p>
                                                                <a href="#">See More <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="empty-space-40"></div>
                                            <div class="separator separator-align-center separator-width-100">
                                                <span class="separator-left">
                                                    <span class="separator-line"></span>
                                                </span>
                                                <h4>Upcoming Events</h4>
                                                <span class="separator-right">
                                                    <span class="separator-line"></span>
                                                </span>
                                            </div>
                                            <div class="empty-space-40"></div>
                                            <div class="timeline timeline-solid timeline-success timeline-text two-columns">
                                                <div class="timeline-wrap">
                                                <?php
                                                    $sql = mysqli_query($con, "SELECT * FROM events ORDER BY date DESC LIMIT 4");
                                                    
                                                    while ($result = mysqli_fetch_array($sql)) 
                                                    {
                                                ?>
                                                    
                                                    <div class="timeline-item">
                                                        <div class="timeline-line el-appear animate-appear"></div>
                                                        <div class="timeline-item-wrap">
                                                            <div class="timeline-badge el-appear animate-appear">
                                                                <a><span>
                                                                    <?php
                                                                        $time = strtotime($result[2]);
                                                                        $newformat = date('M-Y',$time);
                                                                        echo $newformat;
                                                                    ?>
                                                                    </span></a>
                                                            </div>
                                                            <div class="animate-box animated" data-animate="1">
                                                                <div class="timeline-arrow"></div>
                                                                <div class="timeline-content">
                                                                    <span>
                                                                        <p><b>Event Name&nbsp;&nbsp;: </b>&nbsp;&nbsp; <?php echo $result[1];?></p>
                                                                        <p><b>Event Date &nbsp;&nbsp; : </b>&nbsp;&nbsp; <?php echo $result[2];?></p>                                       <p><b>Venue &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </b>&nbsp;&nbsp; <?php echo $result[3];?></p>            
                                                                    </span>
                                                                    <button data-toggle="modal" data-target="#event-2" class="btn btn-info btn-block btn-lg btn-style-round btn-effect-bg-fade-in" type="button">
                                                                    <span>More</span> </button>
                                                                </div>
                                                            </div>
                                                            <div class="column col-md-3 col-sm-6">
                                                                <div class="modal fade" id="event-2" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    <span class="sr-only">Close</span>
                                                                                </button>
                                                                                <center><h4 class="modal-title"><?php echo $result[1];?></h4></center>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p><b>Event Name&nbsp;&nbsp;&nbsp;&nbsp;: </b>&nbsp;&nbsp; <?php echo $result[1];?></p>
                                                                                <p><b>Event Date &nbsp;&nbsp;&nbsp;&nbsp; : </b>&nbsp;&nbsp; <?php echo $result[2];?></p>   
                                                                                <p> <b>Time &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</b>&nbsp;&nbsp; <?php echo $result[4];?></p>
                                                                                <p><b>Venue &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </b>&nbsp;&nbsp; <?php echo $result[3];?></p>
                                                                                <p> <b>Description &nbsp;&nbsp;&nbsp; :</b>&nbsp;&nbsp; <?php echo $result[5];?></p>
                                                                                <p> <b>Particulars &nbsp;&nbsp;&nbsp;&nbsp; :</b>&nbsp;&nbsp; <?php echo $result[6];?></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="empty-space-40"></div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                                    
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="row bg-image section-booking">
                                    <div class="column col-md-2 col-sm-6"></div>
                                    <div class="column col-md-8 col-sm-8" data-fade="1" data-fade-animation="in">
                                        <h3 class="text-center">Booking Service</h3>
                                        <div class="text-block text-center">
                                            <p>
                                                <strong>Opentime: 8:00am -11:30am / 2:00pm-5:30pm / 7:00pm-10:00pm</strong>
                                            </p>
                                        </div>
                                        <div class="text-block">
                                            <form>
                                                <div class="row">
                                                    <div class="column col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <label>Name <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="name" value="" class="form-control" placeholder="Your Name" />
                                                                <span class="form-add-on"><i class="fa fa-user"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="column col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <label>Email <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="email" value="" class="form-control" placeholder="Your Email" />
                                                                <span class="form-add-on"><i class="fa fa-envelope-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="column col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <label>Time <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="time" value="" class="form-control" placeholder="Time" />
                                                                <span class="form-add-on"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="column col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <label>Date <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="date" class="form-control" placeholder="Date" />
                                                                <span class="form-add-on"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="column col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <label>Phone <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="phone" value="" class="form-control" placeholder="Phone" />
                                                                <span class="form-add-on"><i class="None"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="column col-md-12">
                                                        <div class="text-center">
                                                            <button type="submit" class="form-submit">
                                                                <span>Book Now</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="column col-md-2 col-sm-6"></div>
                                    <div class="row-image-bg bg-repeat" data-fixed="1"></div>
                                </div>
                                <!-- <div class="row section-blog">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2 class="text-center">OUR BLOGS</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-2 col-sm-6"></div>
                                                <div class="column col-md-8 col-sm-8">
                                                    <div class="text-block text-center">
                                                        <p>
                                                            YOLIA started as an ambitious idea to promoting the economy and foot traffic in up-and-coming neighborhoods. As the business flourish
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="column col-md-2 col-sm-6"></div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="empty-space-40"></div>
                                            <div class="row no-boder-hentry">
                                                <div class="column col-md-6 col-sm-6">
                                                    <div class="posts-layout-default">
                                                        <article>
                                                            <div class="hentry-wrap">
                                                                <div class="entry-featured ">
                                                                    <a href="#">
                                                                        <img width="700" height="350" src="images/blog/blog.jpg" alt="blog" />
                                                                    </a>
                                                                </div>
                                                                <div class="date-badge">
                                                                    <span class="month-year">Mar</span>
                                                                    <span class="date">01</span>
                                                                </div>
                                                                <div class="entry-info">
                                                                    <div class="entry-header">
                                                                        <h2 class="entry-title">
                                                                            <a href="#">Fusce ac mauris ipsum </a>
                                                                        </h2>
                                                                        <div class="entry-meta">
                                                                            <span class="meta-author">
                                                                                <i class="fa fa-pencil-square-o"></i>
                                                                                <a href="#">sitesao</a>
                                                                            </span>
                                                                            <span class="meta-date">
                                                                                <time datetime="2015-04-03T15:04:50+00:00">
                                                                                    <i class="fa fa-clock-o"></i>March 1, 2015
                                                                                </time>
                                                                            </span>
                                                                            <span class="meta-category">
                                                                                <i class="fa fa-folder-open-o"></i>
                                                                                <a href="#">Fashion</a>, <a href="#">Popular</a>
                                                                            </span>
                                                                            <span class="meta-comment">
                                                                                <i class="fa fa-comment-o"></i>
                                                                                <a href="#" class="meta-comments">7 Comments</a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="entry-content">
                                                                        <p>
                                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia...
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                                <div class="column col-md-6 col-sm-6">
                                                    <div class="posts">
                                                        <div class="posts-layout-default">
                                                            <article>
                                                                <div class="hentry-wrap">
                                                                    <div class="entry-featured ">
                                                                        <a href="#">
                                                                            <img width="700" height="350" src="images/blog/blog2.jpg" alt="blog" />
                                                                        </a>
                                                                    </div>
                                                                    <div class="date-badge">
                                                                        <span class="month-year">Mar</span>
                                                                        <span class="date">14</span>
                                                                    </div>
                                                                    <div class="entry-info">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title">
                                                                                <a href="#">Lorem ipsum dolor sit amet </a>
                                                                            </h2>
                                                                            <div class="entry-meta">
                                                                                <span class="meta-author">
                                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                                    <a href="#">sitesao</a>
                                                                                </span>
                                                                                <span class="meta-date">
                                                                                    <time datetime="2015-04-03T15:04:50+00:00">
                                                                                        <i class="fa fa-clock-o"></i>March 14, 2015
                                                                                    </time>
                                                                                </span>
                                                                                <span class="meta-category">
                                                                                    <i class="fa fa-folder-open-o"></i>
                                                                                    <a href="#">Fashion</a>, <a href="#">Trends</a>
                                                                                </span>
                                                                                <span class="meta-comment">
                                                                                    <i class="fa fa-comment-o"></i>
                                                                                    <a href="#" class="meta-comments">0 Comment</a>
                                                                                </span> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="entry-content">
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia...
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row section-contact-info">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="raw-html">
                                                        <div class="footer-contact-icon">
                                                            <a href="mailto:info@yolia.com">
                                                                <i class="el-appear elegant_icon_mail_alt animate-appear"></i>
                                                                <span class="footer-contact-icon-link">info@yolia.com</span>
                                                                <span class="footer-contact-icon-text">Email</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="raw-html">
                                                        <div class="footer-contact-icon">
                                                            <a href="#">
                                                                <i class="el-appear elegant_icon_pin_alt animate-appear"></i>
                                                                <span class="footer-contact-icon-link">SUITE 1600, DALLAS, TX 725A1</span>
                                                                <span class="footer-contact-icon-text">VISIT</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="raw-html">
                                                        <div class="footer-contact-icon">
                                                            <a href="javascript:void(0)">
                                                                <i class="el-appear elegant_icon_calulator animate-appear"></i>
                                                                <span class="footer-contact-icon-link">+01 2 3456 7891</span>
                                                                <span class="footer-contact-icon-text">PHONE</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-image-bg row-image-bg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php include('footer.php'); ?>

        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>
        <script type='text/javascript' src='js/libs/jquery.js'></script>
        <script type='text/javascript' src='js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='js/libs/easing.min.js'></script>
        <script type='text/javascript' src='js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.parallax.js'></script>

        <script type='text/javascript' src='js/skin-selector.js'></script>

        <script type='text/javascript' src='js/script.js'></script>

    </body>
</html>