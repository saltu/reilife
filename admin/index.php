<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="../images/favicon.png">
        <link rel='stylesheet' href='../css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/skin-selector.css' type='text/css' media='all'/>
<!--        <link rel='stylesheet' href='../css/table.css' type='text/css' media='all'/>-->
        
        
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            function return1()
            {
                alert("You have been canceled");
                window.location ="index.php";
            }
    </script>
    </head>

    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="../images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
        <?php include('header.php');?>
            <div class="content-container no-padding">
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="row">
                                    <div class="column col-md-12">
                                        <div id="revslider" data-autorun="yes" data-duration="10000" class="carousel slide fade dhslider dhslider-custom " data-height="800">
                                            <div class="dhslider-loader">
                                                <div class="fade-loading">
                                                    <i></i><i></i><i></i><i></i>
                                                </div>
                                            </div>
                                            <div class="carousel-inner dhslider-wrap">
                                                <div class="item slider-item active">
                                                    <div class="slide-bg slide-bg-1"></div>
                                                    <div class="slider-overlay"></div>
                                                    <div class="slider-caption caption-align-right">
                                                        <div class="slider-caption-wrap parallax-content">
                                                            <h2 class="slider-heading-text ">REILIFE</h2>
                                                            <div class="slider-caption-text">
                                                                An institute promoting Holistic healing and altunative therapies like Reiki, Acupressure, Sujok, EFT , Flower Remedies etc..
                                                            </div>
                                                            <div class="slider-buttons">
                                                                <a href="services.php" class="btn btn-default btn-success btn-style-solid">
                                                                    SEE MORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item slider-item">
                                                    <div class="slide-bg slide-bg-2"></div>
                                                    <div class="slider-overlay"></div>
                                                    <div class="slider-caption caption-align-left">
                                                        <div class="slider-caption-wrap parallax-content">
                                                            <h2 class="slider-heading-text">VISION OF REILIFE</h2>
                                                            <div class="slider-caption-text">
                                                                Restoring balance and harmony to mind, body and spirit.
                                                            </div>
                                                            <div class="slider-buttons">
                                                                <a href="about-us.php" class="btn btn-default btn-danger btn-style-solid">
                                                                    LEARN MORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ol class="carousel-indicators parallax-content">
                                                <li data-target="#revslider" data-slide-to="0" class="active"></li>
                                                <li data-target="#revslider" data-slide-to="1"></li>
                                            </ol>
                                            <a class="left carousel-control" href="#revslider" data-slide="prev">
                                                <i class="fa fa-angle-left carousel-icon-prev"></i>
                                            </a>
                                            <a class="right carousel-control" href="#revslider" data-slide="next">
                                                <i class="fa fa-angle-right carousel-icon-next"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row section-welcome section-background-bottom">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2 class="text-center">
                                                        WELCOME
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-2 col-sm-6"></div>
                                                <div class="column col-md-8 col-sm-8">
                                                    <div class="text-block">
                                                        <p class="text-center">
                                                            REILIFE, an institute promoting Holistic healing and altunative therapies for restoring balance and harmony to mind, body and spirit.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="column col-md-2 col-sm-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        <?php include('footer.php'); ?>

        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>

        <script type='text/javascript' src='../js/libs/jquery.js'></script>
        <script type='text/javascript' src='../js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='../js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='../js/libs/easing.min.js'></script>
        <script type='text/javascript' src='../js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='../js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='../js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='../js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='../js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.parallax.js'></script>
    
<!--
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->
<!--        <script type='text/javascript' src='../js/content_table.js'></script>-->
        <script type='text/javascript' src='../js/content_table.js'></script>

        <script type='text/javascript' src='../js/skin-selector.js'></script>

        <script type='text/javascript' src='../js/script.js'></script>

    </body>
</html>