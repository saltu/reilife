    
    <footer id="footer" class="footer">
        <div class="footer-info">
            <div class="container">
                <div class="footer-info-wrap">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="copyright">Copyright © 2019 Relife. All Rights Reserved.</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="footer-info-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
        


