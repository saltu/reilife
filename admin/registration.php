<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="../images/favicon.png">
        <link rel='stylesheet' href='../css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/skin-selector.css' type='text/css' media='all'/>
<!--        <link rel='stylesheet' href='../css/table.css' type='text/css' media='all'/>-->
        
        
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            function return1()
            {
                alert("You have been canceled");
                window.location ="index.php";
            }
    </script>
    </head>

    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="../images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
<?php
include('dbconnect.php');
include('header.php');
?>
    <div class="heading-container ">
        <div class="heading-standar">
            <div class="heading-wrap">
               <!--  <div class="container">
                    <div class="page-title">
                        <h1>Registration</h1>
                    </div>
                </div> -->
                <div class="page-breadcrumb">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a class="home" href="#"><span>Home</span></a></li>
                            <li>Registration</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 main-wrap">
                    <div class="main-content">
                        <div class="shop">
                            <div class="column col-md-12 col-sm-12">
                                <div class="accordion">
                                    <div id="accordion-4" class="panel-group">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading panel-icon-square-right">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion-4" href="#accordion-4-10">Registration</a>
                                                </h4>
                                            </div>
                                            <div id="accordion-4-10" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                        <div class="container-full">
                                                            <div class="row">
                                                                <div class="col-md-10 main-wrap">
                                                                    <div class="main-content">
                                                                        <div class="row">
                                                                            <div class="column col-md-10">
                                                                                <div class="container">
                                                                                    <div class="row section-contact">
                                                                                        <div class="column col-md-12 col-sm-12">
                                                                                            <form action='#' method="post">
                                                                                                <div class="row">
                                                                                                    
 
                                      <div class="col-md-4">  
                                        <div class="shop-ordering-select">
                                            <p>
                                                Event<br/>
                                                <span class="form-control-wrap">
                                                    <select name="event" class=" form-control orderby">
                                                        <option value="" selected='selected'> --SELECT EVENT--</option>
                                                        <?php
                                                            $sql = mysqli_query($con, "SELECT id, name FROM events");
                                                            while ($result = mysqli_fetch_array($sql)) {
                                                        ?>
                                                        <option value="<?php echo $result['id'];?>" ><?php echo $result['name'];?></option>
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                    <i></i>
                                                </span>
                                            </p>
                                        </div>
                                      </div>
                                                                                                </div>
                                                                                                <div class="empty-space-40"></div>    
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Name<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="text" name="name" id="name" size="40" class="form-control" required  />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>   
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Gender <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <input type="radio" name="gender" value="male" checked> Male
                                                                                                            &nbsp;&nbsp;&nbsp;
                                                                                                              <input type="radio" name="gender" value="female"> Female
                                                                                                            &nbsp;&nbsp;&nbsp; 
                                                                                                              <input type="radio" name="gender" value="other"> Other 
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-3">
                                                                                                        <p>
                                                                                                            DOB<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input name="dob" type="date" id="dob" size="40" class="form-control" />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div class="col-md-1" ></div>
                                                                                                    <div class="col-md-2" >
                                                                                                        <p>
                                                                                                            Age <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input name="age" id="age" size="40" class="form-control" required />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                        <p>
                                                                                                            Address <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <textarea name="address" id="address" cols="10" rows="4" class="form-control" required ></textarea>
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Occupation<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="text" name="occupation" id="occupation" size="40" class="form-control" required  />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Phone<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="number" name="phone" id="phone" size="40" class="form-control" required />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Whatsapp<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="number" name="whatsapp" id="whatsapp" size="40" class="form-control" required  />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Email ID<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="text" name="mail" id="mail" size="40" class="form-control" required />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-10">
                                                                                                        <p>
                                                                                                            Ailments <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <textarea name="ailments" id="ailments" cols="40" rows="6" class="form-control" required></textarea>
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-1">
                                                                                                        <p>
                                                                                                            Courses <br/>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div class="col-md-10">
                                                                                                            <span class="form-control-wrap">
                                                                                                                <p>a) Reiki</p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                &nbsp;
                                                                                                                <input type="checkbox" name="courses[]" value="r_1st"> 1st degree <br>
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                <input type="checkbox" name="courses[]" value="r_2st"> 2nd degree <br>
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                <input type="checkbox" name="courses[]" value="r_m"> Master degree <br>
                                                                                                                b)
                                                                                                                <input type="checkbox" name="courses[]" value="holistic"> Holistic healing workshop <br>
                                                                                                                c)
                                                                                                                <input type="checkbox" name="courses[]" value="hwb">
                                                                                                                Healing with bliss
                                                                                                            </span>
                                                                                                        
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">    
                                                                                                    <div class="col-md-10">
                                                                                                        <p>
                                                                                                            <input type="submit" value="submit" id="submit" name="submit" class="form-control submit"/>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="response-output display-none"></div>
                                                                                                
                <?php
                  if(isset($_POST["submit"]))
                  {
                      $courses = '';
                      if(!empty($_POST["courses"]))
                       {
                            foreach($_POST["courses"] as $course)
                            {
                                if ($course == '')
                                {
                                    $courses .= '0' . ',';
                                }
                                else
                                {
                                    $courses .= $course . ',';
                                }
                            }
                          
                            $courses = substr($courses, 0, -1);
                       }
                      $sql="INSERT INTO registration VALUES('','$_POST[event]','$_POST[name]','$_POST[address]','$_POST[dob]','$_POST[age]','$_POST[gender]','$_POST[occupation]','$_POST[phone]','$_POST[whatsapp]','$_POST[mail]','$_POST[ailments]', '$courses')";
                      if(mysqli_query($con, $sql))
                      {
                          //echo "<b><font color=red>Registration Successfully Completed.</font></b>";
                          //echo(Confirm());
                          //echo "<script>alert('Registration Successfully Completed')</script>";
                          echo "<script>window.location = 'registration.php'</script>";
                          //header("location:customer.php");
                      }
                      else
                      {
                          echo "<b><font color=red>Registration Failed.</font></b>";
                          die("Error: Server Connection Failed!!!".mysqli_error());
                      }
                  }
                ?>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <table class="table table-bordered shop_table cart">
                                <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">&nbsp;</th>
                                        <th class="product-name text-center">Name</th>
                                        <th class="product-price text-center">Address</th>
                                        <th class="product-quantity text-center">DOB</th>
                                        <th class="product-quantity text-center">Age</th>
                                        <th class="product-quantity text-center">Gender </th>
                                        <th class="product-quantity text-center">Occupation </th>
                                        <th class="product-quantity text-center">Phone</th>
                                        <th class="product-quantity text-center">Whats App</th>
                                        <th class="product-quantity text-center">Mail </th>
                                        <th class="product-quantity text-center">Ailments </th>
                                        <th class="product-quantity text-center">Courses </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php
                                            $sql = mysqli_query($con, "select * from registration");
                                            while ($result = mysqli_fetch_array($sql)) {
                                                
                                                $courses = explode(',', $result[12]);
                                                $selected_courses = '';
                                                
                                                foreach ($courses as $course) 
                                                {
                                                    if ( $course == "r_1st")
                                                    {
                                                        $selected_courses .= '1st degree' . ' | ';
                                                    }
                                                    elseif ( $course == "r_2st" )
                                                    {
                                                        $selected_courses .= '2nd degree' . ' | ';
                                                    }
                                                    elseif ( $course == "r_m" )
                                                    {
                                                        $selected_courses .= 'Master degree' . ' | ';
                                                    }
                                                    elseif ( $course == "holistic" )
                                                    {
                                                        $selected_courses .= 'Holistic healing workshop' . ' | ';
                                                    }
                                                    elseif ( $course == "hwb" )
                                                    {
                                                        $selected_courses .= ' Healing with bliss' . ' | ';
                                                    }
                                                }
                                                $selected_courses = substr($selected_courses, 0, -1);
                                        ?>
                                    
                                                      <tr class="cart_item">
                                                        <td class="product-remove">
                                                            <a onclick="return confirm('Are you sure you want to Delete?');" href="delete-registration.php?id=<?php echo $result[0];?>" class="remove">&times;</a>
                                                        </td>
                                                        <td class="product-thumbnail">
                                                            <center><a href="edit-registration.php?id=<?php echo $result[0];?>" class="fa fa-pencil-square-o"></a></center>
                                                        </td> 
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[2]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[3]; ?></td>  
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[4]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[5]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[6]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[7]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[8]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[9]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[10]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $result[11]; ?></td>
                                                        <td align="center"><?php
                                                            echo "<br>" . $selected_courses; ?></td>    
                                                      </tr>
                                                      <?php
                                            }
                                        ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php include('footer.php'); ?>
<a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>

        <script type='text/javascript' src='../js/libs/jquery.js'></script>
        <script type='text/javascript' src='../js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='../js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='../js/libs/easing.min.js'></script>
        <script type='text/javascript' src='../js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='../js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='../js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='../js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='../js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.parallax.js'></script>
    
<!--
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->
<!--        <script type='text/javascript' src='../js/content_table.js'></script>-->
        <script type='text/javascript' src='../js/content_table.js'></script>

        <script type='text/javascript' src='../js/skin-selector.js'></script>

        <script type='text/javascript' src='../js/script.js'></script>

    </body>
</html>