
    <header class="header-container header-type-default header-default-center header-navbar-default header-absolute header-transparent">
        <div class="navbar-container header-colour">
            <div class="navbar navbar-default navbar-scroll-fixed">
                <div class="navbar-default-wrap ">
                    <div class="container">
                        <div class="navbar-wrap">
                            <div class="navbar-header">
                                <button data-target=".primary-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar bar-top"></span>
                                    <span class="icon-bar bar-middle"></span>
                                    <span class="icon-bar bar-bottom"></span>
                                </button>
                                <a class="cart-icon-mobile" href="#"><i class="elegant_icon_bag"></i> <span>0</span></a>
                                <a class="navbar-brand" title="Reilife" href="index.php">
                                    <img class="logo" alt="Reilife" src="../images/logo-white.png">
                                    <img class="logo-fixed" alt="Reilife" src="../images/logo-fixed.png">
                                    <img class="logo-mobile" alt="Reilife" src="../images/logo-mobile.png">
                                </a>
                            </div>
                            <nav class="collapse navbar-collapse primary-navbar-collapse">
                                <ul class="nav navbar-nav primary-nav">
                                    <li class="menu-item-navbar-brand">
                                        <a class="navbar-brand" href="index.php">
                                            <img class="logo" alt="Reilife" src="../images/logo-white.png">
                                            <img class="logo-fixed" alt="Reilife" src="../images/logo-fixed.png">
                                            <img class="logo-mobile" alt="Reilife" src="../images/logo-mobile.png">
                                        </a>
                                    </li>
                                    <li class="menu-item-has-children dropdown">
                                        <a title="Home" href="index.php" class="dropdown-hover">
                                            <span class="underline">Home</span> <span class="caret"></span>
                                        </a>
                                    </li>
                                    <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown bg_1">
                                        <a title="Services" href="events.php" class="dropdown-hover">
                                            <span class="underline">Events</span> <span class="caret"></span>
                                        </a>
                                    </li>
                                    <!-- <li class="menu-item-has-children dropdown">
                                        <a title="Blog" href="#" class="dropdown-hover">
                                            <span class="underline">Blog</span> <span class="caret"></span>
                                        </a>
                                    </li> -->

                                    <li class="menu-item-has-children dropdown">
                                        <a title="Events" href="registration.php" class="dropdown-hover">
                                            <span class="underline">Registration</span> <span class="caret"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
            