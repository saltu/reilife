<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="../images/favicon.png">
        <link rel='stylesheet' href='../css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='../css/skin-selector.css' type='text/css' media='all'/>
<!--        <link rel='stylesheet' href='../css/table.css' type='text/css' media='all'/>-->
        
        
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            function return1()
            {
                alert("You have been canceled");
                window.location ="index.php";
            }
    </script>
    </head>

    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="../images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
            <?php
include('dbconnect.php');
include('header.php');
$id = $_GET["id"];
?>
    <div class="heading-container ">
        <div class="heading-standar">
            <div class="heading-wrap">
                <!-- <div class="container">
                    <div class="page-title">
                        <h1>Registration</h1>
                    </div>
                </div> -->
                <div class="page-breadcrumb">
                    <div class="container">
                        <ul class="breadcrumb">
                            <li><a class="home" href="index.php"><span>Home</span></a></li>
                            <li><a class="home" href="registration.php"><span>Registration</span></a></li>
                            <li>Edit</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 main-wrap">
                    <div class="main-content">
                        <div class="shop">
                            <div class="column col-md-12 col-sm-12">
                                <div class="accordion">
                                    <div id="accordion-4" class="panel-group">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading panel-icon-square-right">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion-4" href="#accordion-4-10">Registration</a>
                                                </h4>
                                            </div>
                                            <div id="accordion-4-10" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                        <div class="container-full">
                                                            <div class="row">
                                                                <div class="col-md-10 main-wrap">
                                                                    <div class="main-content">
                                                                        <div class="row">
                                                                            <div class="column col-md-10">
                                                                                <div class="container">
                                                                                    <div class="row section-contact">
                                                                                        <div class="column col-md-12 col-sm-12">
                                                                                            <form action='#' method="post">
                                                                                                <div class="row">
                                                                                                    
                                            <?php
                                              $sql=mysqli_query($con, "SELECT * FROM registration WHERE id= '$id'");
                                              $result = mysqli_fetch_array($sql);
                                            ?>
                                                                                                                                                           
                                      <div class="col-md-4">  
                                        <div class="shop-ordering-select">
                                            <p>
                                                Event<br/>
                                                <span class="form-control-wrap">
                                                    <select name="event" class=" form-control orderby">
                                                        <?php
                                                            $sql1 = mysqli_query($con, "SELECT id, name FROM events");
                                                            while ($result1 = mysqli_fetch_array($sql1)) {
                                                                if ($result1['id'] == $result[1])
                                                                {
                                                        ?>
                                                                    <option value="<?php echo $result1['id'];?>" selected='selected'> <?php echo $result1['name']?></option>
                                                            <?php        
                                                                }
                                                                else
                                                                {
                                                            ?>
                                                                    <option value="<?php echo $result1['id'];?>" > <?php echo $result1['name'];?></option>
                                                            <?php        
                                                                }
                                                            ?>
                                                        
                                                        <?php
                                                            }
                                                        ?>
                                                    </select>
                                                    <i></i>
                                                </span>
                                            </p>
                                        </div>
                                      </div>
                                                                                                </div>
                                                                                                <div class="empty-space-40"></div>    
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Name<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="text" name="name" value= "<?php echo $result[2]?>" id="name" size="40" class="form-control" required  />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>   
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Gender <br/>
                                                            <span class="form-control-wrap">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                                <input type="radio" name="gender" <?=$result[6]=="male" ? "checked" : ""?> value="male"  > Male &nbsp;&nbsp;&nbsp;
                                                                <input type="radio" name="gender" <?=$result[6]=="female" ? "checked" : ""?>  value="female"  > Female
                                                            &nbsp;&nbsp;&nbsp; 
                                                                <input type="radio" name="gender" <?=$result[6]=="other" ? "checked" : ""?> value="other" > Other 
                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-3">
                                                                                                        <p>
                                                                                                            DOB<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input name="dob" type="date"
                                                                                                                value= "<?php echo $result[4]?>" id="dob" size="40" class="form-control" />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div class="col-md-1" ></div>
                                                                                                    <div class="col-md-2" >
                                                                                                        <p>
                                                                                                            Age <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input name="age" id="age" size="40" value= "<?php echo $result[5]?>" class="form-control" required />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                        <p>
                                                                                                            Address <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <textarea name="address" id="address" cols="10" rows="4" class="form-control" required > <?php echo $result[3]?> </textarea>
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Occupation<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="text" name="occupation" id="occupation" size="40" value= "<?php echo $result[7]?>" class="form-control" required  />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Phone<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="number" name="phone" id="phone" size="40" value= "<?php echo $result[8]?>" class="form-control" required />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Whatsapp<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="number" name="whatsapp" id="whatsapp" size="40" value= "<?php echo $result[9]?>"  class="form-control" required  />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-4">
                                                                                                        <p>
                                                                                                            Email ID<br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <input type="text" name="mail" id="mail" size="40" value= "<?php echo $result[10]?>" class="form-control" required />
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-10">
                                                                                                        <p>
                                                                                                            Ailments <br/>
                                                                                                            <span class="form-control-wrap">
                                                                                                                <textarea name="ailments" id="ailments" cols="40" rows="6" class="form-control" required><?php echo $result[11]?></textarea>
                                                                                                            </span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-md-1">
                                                                                                        <p>
                                                                                                            Courses <br/>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <?php        
                                                                                                       $courses = explode(',', $result[12]);
                                                                                                       $allcourses = array("r_1st","r_2st","r_m","holistic","hwb");

                                                                                                    $selected_courses = array('');
                                                                                                    $count = count($courses);
                                                                                                        foreach ($allcourses as $course) 
                                                                                                        {
                                                                                                            if (in_array($course,$courses))
                                                                                                            {
                                                                                                                array_push($selected_courses, $course);
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                array_push($selected_courses, '0' );
                                                                                                            }
                                                                                                        }             
                                                                                                    ?>
                                                                                                    <div class="col-md-10">
                                                                                                            <span class="form-control-wrap">
                                                                                                                <p>a) Reiki</p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                &nbsp;
                                                                                                                <input type="checkbox" name="courses[]" value="r_1st"
                                                                                                                <?php echo ($selected_courses[1]== "r_1st" ? 'checked' : '');?> 
                                                                                                                       > 1st degree <br>
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                <input type="checkbox" name="courses[]" value="r_2st" <?php echo ($selected_courses[2]== "r_2st" ? 'checked' : '');?> > 2nd degree <br>
                                                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                <input type="checkbox" name="courses[]" value="r_m" <?php echo ($selected_courses[3]== "r_m" ? 'checked' : '');?>> Master degree <br>
                                                                                                                b)
                                                                                                                <input type="checkbox" name="courses[]" value="holistic" <?php echo ($selected_courses[4]== "holistic" ? 'checked' : '');?>> Holistic healing workshop <br>
                                                                                                                c)
                                                                                                                <input type="checkbox" name="courses[]" value="hwb" <?php echo ($selected_courses[5]== "hwb" ? 'checked' : '');?> >
                                                                                                                Healing with bliss
                                                                                                            </span>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-5">
                                                                                                        <p><input type="reset" value="Cancel" id="cancel" name="submit" onclick="return1();" class="form-control submit" style=" background-color:#2cf5dd; border-color:#2cf5dd;!important"/>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div class="col-md-5">
                                                                                                        <p>
                                                                                                            <input type="submit" value="submit" id="submit" name="submit" class="form-control submit"/> 
                                                                                                        </p>
                                                                                                    </div>
                                                                                                <div class="response-output display-none"></div>
                                                                                                
             <?php
                  if(isset($_POST["submit"]))
                  {
                      $courses = '';
                      if(!empty($_POST["courses"]))
                       {
                            foreach($_POST["courses"] as $course)
                            {
                                if ($course == '')
                                {
                                    $courses .= '0' . ',';
                                }
                                else
                                {
                                    $courses .= $course . ',';
                                }
                            }
                            $courses = substr($courses, 0, -1);
                       }
                      
                      $sql="update registration set event = '$_POST[event]', name = '$_POST[name]', address = '$_POST[address]', dob = '$_POST[dob]', age = '$_POST[age]', gender = '$_POST[gender]', occupation = '$_POST[occupation]', phone = '$_POST[phone]', whatsapp = '$_POST[whatsapp]', mail = '$_POST[mail]', ailments = '$_POST[ailments]', courses = '$courses' where id = '$id'";
                      
                      if(mysqli_query($con, $sql))
                      {
                          echo "<script>window.location = 'registration.php'</script>";
                      }
                      else
                      {
                          echo "<b><font color=red>Registration Failed.</font></b>";
                          die("Error: Server Connection Failed!!!".mysqli_error());
                      }
                  }
                ?>
                                                                                                
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php include('footer.php'); ?>
<a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>

        <script type='text/javascript' src='../js/libs/jquery.js'></script>
        <script type='text/javascript' src='../js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='../js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='../js/libs/easing.min.js'></script>
        <script type='text/javascript' src='../js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='../js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='../js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='../js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='../js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='../js/libs/jquery.parallax.js'></script>
    
<!--
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
-->
<!--        <script type='text/javascript' src='../js/content_table.js'></script>-->
        <script type='text/javascript' src='../js/content_table.js'></script>

        <script type='text/javascript' src='../js/skin-selector.js'></script>

        <script type='text/javascript' src='../js/script.js'></script>

    </body>
</html>