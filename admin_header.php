<!doctype html>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
            <header class="header-container header-type-default header-default-center header-navbar-default header-absolute header-transparent">
                <div class="navbar-container header-colour">
                    <div class="navbar navbar-default navbar-scroll-fixed">
                        <div class="navbar-default-wrap ">
                            <div class="container">
                                <div class="navbar-wrap">
                                    <div class="navbar-header">
                                        <button data-target=".primary-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar bar-top"></span>
                                            <span class="icon-bar bar-middle"></span>
                                            <span class="icon-bar bar-bottom"></span>
                                        </button>
                                        <a class="cart-icon-mobile" href="#"><i class="elegant_icon_bag"></i> <span>0</span></a>
                                        <a class="navbar-brand" title="Reilife" href="index.html">
                                            <img class="logo" alt="Reilife" src="images/logo-white.png">
                                            <img class="logo-fixed" alt="Reilife" src="images/logo-fixed.png">
                                            <img class="logo-mobile" alt="Reilife" src="images/logo-mobile.png">
                                        </a>
                                    </div>
                                    <nav class="collapse navbar-collapse primary-navbar-collapse">
                                        <ul class="nav navbar-nav primary-nav">
                                            <li class="menu-item-navbar-brand">
                                                <a class="navbar-brand" href="index.html">
                                                    <img class="logo" alt="Reilife" src="images/logo-white.png">
                                                    <img class="logo-fixed" alt="Reilife" src="images/logo-fixed.png">
                                                    <img class="logo-mobile" alt="Reilife" src="images/logo-mobile.png">
                                                </a>
                                            </li>
                                            <li class="menu-item-has-children dropdown">
                                                <a title="Home" href="index.php" class="dropdown-hover">
                                                    <span class="underline">Home</span> <span class="caret"></span>
                                                </a>
                                            </li>
                                            <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown bg_1">
                                                <a title="Services" href="#" class="dropdown-hover">
                                                    <span class="underline">Events</span> <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="menu-item-has-children mega-col-4 dropdown">
                                                        <ul class="dropdown-menu">
                                                            <li><a title="Reiki" href="#">View Events</a></li>
                                                            <li><a title="TOFPI" href="#">Add Events</a></li>
                                                            <li><a title="NLP" href="#">Delete Events</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="visible-lg-inline mega-col-4"></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children dropdown">
                                                <a title="Blog" href="blog.php" class="dropdown-hover">
                                                    <span class="underline">Blog</span> <span class="caret"></span>
                                                </a>
                                            </li>

                                            <li class="menu-item-has-children dropdown">
                                                <a title="Events" href="event.php" class="dropdown-hover">
                                                    <span class="underline">Registration</span> <span class="caret"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            