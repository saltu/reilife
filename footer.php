    <div class="footer-widget">
        <div class="container">
            <div class="footer-widget-wrap">
                <div class="row">
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget widget_text">
                            <div class="textwidget">
                                <p>
                                    <img src="images/logo-white-small.png" alt=""/>
                                    <br /><br /><br />
                                    <!-- Litora etiam blandit curabitur pretium ac a risus a, velit curabitur platea dolore magna aliquam erat volutpat. -->
                                </p>
                            </div>
                        </div>
                        <div class="widget social-widget">
                            <div class="social-widget-wrap social-widget-outlined">
                                <a href="#" title="Facebook" target="_blank">
                                    <i class="fa fa-facebook facebook-bg-hover facebook-outlined"></i>
                                </a>
                                <a href="#" title="Twitter" target="_blank">
                                    <i class="fa fa-twitter twitter-bg-hover twitter-outlined"></i>
                                </a>
                                <a href="#" title="Google+" target="_blank">
                                    <i class="fa fa-google-plus google-plus-bg-hover google-plus-outlined"></i>
                                </a>
                                <a href="#" title="Pinterest" target="_blank">
                                    <i class="fa fa-pinterest pinterest-bg-hover pinterest-outlined"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget">
                            <h3 class="widget-title">
                                <span>Services</span>
                            </h3>
                            <ul class="posts-thumbnail-list">
                                <li>
                                    <!-- <div class="posts-thumbnail-image">
                                        <a href="#">
                                            <img width="600" height="450" src="images/blog/blog3.jpg" alt="blog" />
                                        </a>
                                    </div> -->
                                    <div class="posts-thumbnail-content">
                                        <h4>
                                        <li><a href="services.php#Reiki">Reiki</a></li>
                                        <li><a href="services.php#Teachings of Pranashakty">Teachings of Pranashakty</a></li>
                                        <li><a href="services.php#Hypnotherapy">Hypnotherapy</a></li>
                                        <li><a href="services.php#NLP">NLP</a></li>
                                        <li><a href="services.php#BACHFLOWER">BACHFLOWER Remedies</a></li>
                                        <li><a href="services.php#BAT">BAT</a></li>
                                        <li><a href="services.php#Reflexology">Reflexology & Sujok</a></li>

                                        </h4>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget">
                            <h3 class="widget-title">
                                <span>Menu</span>
                            </h3>
                            <div class="recent-tweets">
                                <ul style="font-size: 16px;">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="services.php">Services</a></li>
                                    <!-- <li><a href="blog.php">Blog</a></li>
                                    <li><a href="event.php">Events</a></li> -->
                                    <li><a href="about-us.php">About Us</a></li>
                                    <li><a href="contact.php">Contact Us</a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-widget-col col-md-3 col-sm-6">
                        <div class="widget">
                            <h3 class="widget-title">
                                <span>Get in touch</span>
                            </h3>
                            <div class="textwidget">
                                <p><i class="fa fa-map-marker"></i> A1, 1st Floor
                                                                Kaustubham, Nandnath Kochako Rd
                                                                Chakaraparambu
                                                                Eklm - 32</p>
                                <p><i class="fa fa-phone"></i>+91 9847494961, +91 9747910823</p>
                                <p><i class="fa fa-envelope"></i> <a href="mailto:info@reilife.com">info@reilife.com</a></p>
                                <!-- <p><i class="fa fa-fax"></i> (012) 1234 7824</p>
                                <p><i class="fa fa-clock-o"></i> Mon - Sat: 9:00 - 19:00</p> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="footer" class="footer">
        <div class="footer-info">
            <div class="container">
                <div class="footer-info-wrap">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="copyright">Copyright © 2019 Relife. All Rights Reserved.</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="footer-info-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

       