<?php include('header.php');?>
            <div class="content-container no-padding">
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="row">
                                    <div class="column col-md-12">
                                        <div id="revslider" data-autorun="yes" data-duration="10000" class="carousel slide fade dhslider dhslider-custom " data-height="800">
                                            <div class="dhslider-loader">
                                                <div class="fade-loading">
                                                    <i></i><i></i><i></i><i></i>
                                                </div>
                                            </div>
                                            <div class="carousel-inner dhslider-wrap">
                                                <div class="item slider-item active">
                                                    <div class="slide-bg slide-bg-1"></div>
                                                    <div class="slider-overlay"></div>
                                                    <div class="slider-caption caption-align-right">
                                                        <div class="slider-caption-wrap parallax-content">
                                                            <h2 class="slider-heading-text ">REILIFE</h2>
                                                            <div class="slider-caption-text">
                                                                An institute promoting Holistic healing and altunative therapies like Reiki, Acupressure, Sujok, EFT , Flower Remedies etc..
                                                            </div>
                                                            <div class="slider-buttons">
                                                                <a href="services.php" class="btn btn-default btn-success btn-style-solid">
                                                                    SEE MORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item slider-item">
                                                    <div class="slide-bg slide-bg-2"></div>
                                                    <div class="slider-overlay"></div>
                                                    <div class="slider-caption caption-align-left">
                                                        <div class="slider-caption-wrap parallax-content">
                                                            <h2 class="slider-heading-text">VISION OF REILIFE</h2>
                                                            <div class="slider-caption-text">
                                                                Restoring balance and harmony to mind, body and spirit.
                                                            </div>
                                                            <div class="slider-buttons">
                                                                <a href="about-us.php" class="btn btn-default btn-danger btn-style-solid">
                                                                    LEARN MORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ol class="carousel-indicators parallax-content">
                                                <li data-target="#revslider" data-slide-to="0" class="active"></li>
                                                <li data-target="#revslider" data-slide-to="1"></li>
                                            </ol>
                                            <a class="left carousel-control" href="#revslider" data-slide="prev">
                                                <i class="fa fa-angle-left carousel-icon-prev"></i>
                                            </a>
                                            <a class="right carousel-control" href="#revslider" data-slide="next">
                                                <i class="fa fa-angle-right carousel-icon-next"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row section-welcome section-background-bottom">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2 class="text-center">
                                                        WELCOME
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-2 col-sm-6"></div>
                                                <div class="column col-md-8 col-sm-8">
                                                    <div class="text-block">
                                                        <p class="text-center">
                                                            REILIFE, an institute promoting Holistic healing and altunative therapies for restoring balance and harmony to mind, body and spirit.
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="column col-md-2 col-sm-6"></div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row welcome-items">
                                                <div class="column col-md-4 col-sm-4" data-fade="1" data-fade-animation="in">
                                                    <div class="single-image image-center">
                                                        <img width="500" height="250" src="images/services/service-1.jpg" class="box-default" alt="service-1"/>
                                                    </div>
                                                    <div class="box box-custom-margin-padding">
                                                        <h4>VISION</h4>
                                                        <div class="text-block text-center">
                                                            <p>
                                                                Ut ultricies quis nulla non lacinia. Suspendisse posuere orci enim, ac imperdiet diam mattis nec. Etiam ante nisi.
                                                            </p>
                                                            <p>
                                                                <a href="#">See More <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4" data-fade="1" data-fade-animation="in">
                                                    <div class="single-image image-center">
                                                        <img width="500" height="250" src="images/services/service-2.jpg" class="box-default" alt="service-2"/>
                                                    </div>
                                                    <div class="box box-custom-margin-padding">
                                                        <h4>MISSION</h4>
                                                        <div class="text-block text-center">
                                                            <p>
                                                                Quisque ullamcorper libero urna. Integer in lacus gravida, pharetra neque quis, dictum diam. Donec faucibus varius.
                                                            </p>
                                                            <p>
                                                                <a href="#">See More <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4" data-fade="1" data-fade-animation="in">
                                                    <div class="single-image image-center">
                                                        <img width="500" height="250" src="images/services/service-3.jpg" class="box-default" alt="service-3"/>
                                                    </div>
                                                    <div class="box box-custom-margin-padding">
                                                        <h4>VALUES</h4>
                                                        <div class="text-block text-center">
                                                            <p>
                                                                Nam id porttitor lacus. Fusce cursus porttitor pulvinar. Maecenas risus nunc, ornare et ultricies id, vestibulum in metus.
                                                            </p>
                                                            <p>
                                                                <a href="#">See More <i class="fa fa-arrow-circle-o-right"></i></a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="empty-space-40"></div>
                                            <div class="separator separator-align-center separator-width-100">
                                                <span class="separator-left">
                                                    <span class="separator-line"></span>
                                                </span>
                                                <h4>Upcoming Events</h4>
                                                <span class="separator-right">
                                                    <span class="separator-line"></span>
                                                </span>
                                            </div>
                                            <div class="empty-space-40"></div>
                                            <div class="timeline timeline-solid timeline-success timeline-text two-columns">
                                                <div class="timeline-wrap">
                                                    <div class="timeline-item">
                                                        <div class="timeline-line el-appear animate-appear"></div>
                                                        <div class="timeline-item-wrap">
                                                            <div class="timeline-badge el-appear animate-appear">
                                                                <a><span>JAN 2014</span></a>
                                                            </div>
                                                            <div class="animate-box animated" data-animate="1">
                                                                <div class="timeline-arrow"></div>
                                                                <div class="timeline-content">
                                                                    <p>
                                                                        Fusce dictum, nisi vel dignissim placerat, magna ligula venenatis felis, eget eleifend neque mi vel nulla. Proin vel accumsan mi. Fusce ultrices euismod orci ut tempor. Sed luctus ex ac purus vulputate, vel ultricies nulla molestie. Aliquam efficitur ac diam fringilla aliquet.  
                                                                    </p>
                                                                    <button data-toggle="modal" data-target="#event-1" class="btn btn-info btn-block btn-lg btn-style-round btn-effect-bg-fade-in" type="button">
                                                                    <span>Modal Large</span> </button>
                                                                </div> 
                                                            </div>                                                        
                                                            <div class="column col-md-3 col-sm-6">
                                                                <div class="modal fade" id="event-1" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    <span class="sr-only">Close</span>
                                                                                </button>
                                                                                <h4 class="modal-title">Event 1</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>
                                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius venenatis sapien, id interdum metus condimentum vitae. Quisque sit amet egestas leo. Cras quam ex, tristique nec tincidunt mattis, malesuada sit amet tellus.
                                                                                </p>
                                                                                <p>
                                                                                    Cras ac consectetur enim, laoreet iaculis erat. Curabitur mi mi, suscipit a efficitur consectetur, consequat in sapien. Integer mi mauris, hendrerit et eros id, commodo vestibulum odio. Maecenas magna felis, sodales a erat ut, fringilla bibendum risus. Donec efficitur feugiat nibh, eget hendrerit magna. Pellentesque fermentum, elit in auctor ultrices, metus massa accumsan eros, non egestas turpis eros ut metus
                                                                                </p>
                                                                                <p>
                                                                                    Nulla facilisi. Donec justo metus, ullamcorper at erat ac, auctor efficitur eros. Nulla aliquam libero nec fermentum commodo.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="empty-space-40"></div>
                                                            </div>                                   
                                                        </div>
                                                    </div>

                                                    <div class="timeline-item">
                                                        <div class="timeline-line el-appear animate-appear"></div>
                                                        <div class="timeline-item-wrap">
                                                            <div class="timeline-badge el-appear animate-appear">
                                                                <a><span>FEB 2014</span></a>
                                                            </div>
                                                            <div class="animate-box animated" data-animate="1">
                                                                <div class="timeline-arrow"></div>
                                                                <div class="timeline-content">
                                                                    <p>
                                                                        Fusce dictum, nisi vel dignissim placerat, magna ligula venenatis felis, eget eleifend neque mi vel nulla. Proin vel accumsan mi. Fusce ultrices euismod orci ut tempor. Sed luctus ex ac purus vulputate, vel ultricies nulla molestie. Aliquam efficitur ac diam fringilla aliquet.
                                                                    </p>
                                                                    <button data-toggle="modal" data-target="#event-2" class="btn btn-info btn-block btn-lg btn-style-round btn-effect-bg-fade-in" type="button">
                                                                    <span>Modal Large</span> </button>
                                                                </div>
                                                            </div>
                                                            <div class="column col-md-3 col-sm-6">
                                                                <div class="modal fade" id="event-2" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    <span class="sr-only">Close</span>
                                                                                </button>
                                                                                <h4 class="modal-title">Event 2</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>
                                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius venenatis sapien, id interdum metus condimentum vitae. Quisque sit amet egestas leo. Cras quam ex, tristique nec tincidunt mattis, malesuada sit amet tellus.
                                                                                </p>
                                                                                <p>
                                                                                    Cras ac consectetur enim, laoreet iaculis erat. Curabitur mi mi, suscipit a efficitur consectetur, consequat in sapien. Integer mi mauris, hendrerit et eros id, commodo vestibulum odio. Maecenas magna felis, sodales a erat ut, fringilla bibendum risus. Donec efficitur feugiat nibh, eget hendrerit magna. Pellentesque fermentum, elit in auctor ultrices, metus massa accumsan eros, non egestas turpis eros ut metus
                                                                                </p>
                                                                                <p>
                                                                                    Nulla facilisi. Donec justo metus, ullamcorper at erat ac, auctor efficitur eros. Nulla aliquam libero nec fermentum commodo.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="empty-space-40"></div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="timeline-item">
                                                        <div class="timeline-line el-appear animate-appear"></div>
                                                        <div class="timeline-item-wrap">
                                                            <div class="timeline-badge el-appear animate-appear">
                                                                <a><span>MAR 2014</span></a>
                                                            </div>
                                                            <div class="animate-box animated" data-animate="1">
                                                                <div class="timeline-arrow"></div>
                                                                <div class="timeline-content">
                                                                    <p>
                                                                        Fusce dictum, nisi vel dignissim placerat, magna ligula venenatis felis, eget eleifend neque mi vel nulla. Proin vel accumsan mi. Fusce ultrices euismod orci ut tempor. Sed luctus ex ac purus vulputate, vel ultricies nulla molestie. Aliquam efficitur ac diam fringilla aliquet.
                                                                    </p>
                                                                    <button data-toggle="modal" data-target="#event-3" class="btn btn-info btn-block btn-lg btn-style-round btn-effect-bg-fade-in" type="button">
                                                                    <span>Modal Large</span> </button>
                                                                </div>
                                                            </div>
                                                            <div class="column col-md-3 col-sm-6">
                                                                <div class="modal fade" id="event-3" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    <span class="sr-only">Close</span>
                                                                                </button>
                                                                                <h4 class="modal-title">Event 3</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>
                                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius venenatis sapien, id interdum metus condimentum vitae. Quisque sit amet egestas leo. Cras quam ex, tristique nec tincidunt mattis, malesuada sit amet tellus.
                                                                                </p>
                                                                                <p>
                                                                                    Cras ac consectetur enim, laoreet iaculis erat. Curabitur mi mi, suscipit a efficitur consectetur, consequat in sapien. Integer mi mauris, hendrerit et eros id, commodo vestibulum odio. Maecenas magna felis, sodales a erat ut, fringilla bibendum risus. Donec efficitur feugiat nibh, eget hendrerit magna. Pellentesque fermentum, elit in auctor ultrices, metus massa accumsan eros, non egestas turpis eros ut metus
                                                                                </p>
                                                                                <p>
                                                                                    Nulla facilisi. Donec justo metus, ullamcorper at erat ac, auctor efficitur eros. Nulla aliquam libero nec fermentum commodo.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="empty-space-40"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-item">
                                                        <div class="timeline-line el-appear animate-appear"></div>
                                                        <div class="timeline-item-wrap">
                                                            <div class="timeline-badge el-appear animate-appear">
                                                                <a><span>APR 2014</span></a>
                                                            </div>
                                                            <div class="animate-box animated" data-animate="1">
                                                                <div class="timeline-arrow"></div>
                                                                <div class="timeline-content">
                                                                    <p>
                                                                        Fusce dictum, nisi vel dignissim placerat, magna ligula venenatis felis, eget eleifend neque mi vel nulla. Proin vel accumsan mi. Fusce ultrices euismod orci ut tempor. Sed luctus ex ac purus vulputate, vel ultricies nulla molestie. Aliquam efficitur ac diam fringilla aliquet.
                                                                    </p>
                                                                    <button data-toggle="modal" data-target="#event-4" class="btn btn-info btn-block btn-lg btn-style-round btn-effect-bg-fade-in" type="button">
                                                                    <span>Modal Large</span> </button>
                                                                </div>
                                                            </div>
                                                            <div class="column col-md-3 col-sm-6">
                                                                <div class="modal fade" id="event-4" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-center modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                    <span class="sr-only">Close</span>
                                                                                </button>
                                                                                <h4 class="modal-title">Event 4</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>
                                                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius venenatis sapien, id interdum metus condimentum vitae. Quisque sit amet egestas leo. Cras quam ex, tristique nec tincidunt mattis, malesuada sit amet tellus.
                                                                                </p>
                                                                                <p>
                                                                                    Cras ac consectetur enim, laoreet iaculis erat. Curabitur mi mi, suscipit a efficitur consectetur, consequat in sapien. Integer mi mauris, hendrerit et eros id, commodo vestibulum odio. Maecenas magna felis, sodales a erat ut, fringilla bibendum risus. Donec efficitur feugiat nibh, eget hendrerit magna. Pellentesque fermentum, elit in auctor ultrices, metus massa accumsan eros, non egestas turpis eros ut metus
                                                                                </p>
                                                                                <p>
                                                                                    Nulla facilisi. Donec justo metus, ullamcorper at erat ac, auctor efficitur eros. Nulla aliquam libero nec fermentum commodo.
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="empty-space-40"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         </div>
                                    </div>
                                </div>
                                <div class="row bg-image section-booking">
                                    <div class="column col-md-2 col-sm-6"></div>
                                    <div class="column col-md-8 col-sm-8" data-fade="1" data-fade-animation="in">
                                        <h3 class="text-center">Booking Service</h3>
                                        <div class="text-block text-center">
                                            <p>
                                                <strong>Opentime: 8:00am -11:30am / 2:00pm-5:30pm / 7:00pm-10:00pm</strong>
                                            </p>
                                        </div>
                                        <div class="text-block">
                                            <form>
                                                <div class="row">
                                                    <div class="column col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <label>Name <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="name" value="" class="form-control" placeholder="Your Name" />
                                                                <span class="form-add-on"><i class="fa fa-user"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="column col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <label>Email <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="email" value="" class="form-control" placeholder="Your Email" />
                                                                <span class="form-add-on"><i class="fa fa-envelope-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="column col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <label>Time <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="time" value="" class="form-control" placeholder="Time" />
                                                                <span class="form-add-on"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="column col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <label>Date <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="date" class="form-control" placeholder="Date" />
                                                                <span class="form-add-on"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="column col-md-4 col-sm-4">
                                                        <div class="form-group">
                                                            <label>Phone <span class="required">*</span></label>
                                                            <div class="form-input">
                                                                <input type="text" name="phone" value="" class="form-control" placeholder="Phone" />
                                                                <span class="form-add-on"><i class="None"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="column col-md-12">
                                                        <div class="text-center">
                                                            <button type="submit" class="form-submit">
                                                                <span>Book Now</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="column col-md-2 col-sm-6"></div>
                                    <div class="row-image-bg bg-repeat" data-fixed="1"></div>
                                </div>
                                <div class="row section-blog">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2 class="text-center">OUR BLOGS</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-2 col-sm-6"></div>
                                                <div class="column col-md-8 col-sm-8">
                                                    <div class="text-block text-center">
                                                        <p>
                                                            YOLIA started as an ambitious idea to promoting the economy and foot traffic in up-and-coming neighborhoods. As the business flourish
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="column col-md-2 col-sm-6"></div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="empty-space-40"></div>
                                            <div class="row no-boder-hentry">
                                                <div class="column col-md-6 col-sm-6">
                                                    <div class="posts-layout-default">
                                                        <article>
                                                            <div class="hentry-wrap">
                                                                <div class="entry-featured ">
                                                                    <a href="#">
                                                                        <img width="700" height="350" src="images/blog/blog.jpg" alt="blog" />
                                                                    </a>
                                                                </div>
                                                                <div class="date-badge">
                                                                    <span class="month-year">Mar</span>
                                                                    <span class="date">01</span>
                                                                </div>
                                                                <div class="entry-info">
                                                                    <div class="entry-header">
                                                                        <h2 class="entry-title">
                                                                            <a href="#">Fusce ac mauris ipsum </a>
                                                                        </h2>
                                                                        <div class="entry-meta">
                                                                            <span class="meta-author">
                                                                                <i class="fa fa-pencil-square-o"></i>
                                                                                <a href="#">sitesao</a>
                                                                            </span>
                                                                            <span class="meta-date">
                                                                                <time datetime="2015-04-03T15:04:50+00:00">
                                                                                    <i class="fa fa-clock-o"></i>March 1, 2015
                                                                                </time>
                                                                            </span>
                                                                            <span class="meta-category">
                                                                                <i class="fa fa-folder-open-o"></i>
                                                                                <a href="#">Fashion</a>, <a href="#">Popular</a>
                                                                            </span>
                                                                            <span class="meta-comment">
                                                                                <i class="fa fa-comment-o"></i>
                                                                                <a href="#" class="meta-comments">7 Comments</a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="entry-content">
                                                                        <p>
                                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia...
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                                <div class="column col-md-6 col-sm-6">
                                                    <div class="posts">
                                                        <div class="posts-layout-default">
                                                            <article>
                                                                <div class="hentry-wrap">
                                                                    <div class="entry-featured ">
                                                                        <a href="#">
                                                                            <img width="700" height="350" src="images/blog/blog2.jpg" alt="blog" />
                                                                        </a>
                                                                    </div>
                                                                    <div class="date-badge">
                                                                        <span class="month-year">Mar</span>
                                                                        <span class="date">14</span>
                                                                    </div>
                                                                    <div class="entry-info">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title">
                                                                                <a href="#">Lorem ipsum dolor sit amet </a>
                                                                            </h2>
                                                                            <div class="entry-meta">
                                                                                <span class="meta-author">
                                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                                    <a href="#">sitesao</a>
                                                                                </span>
                                                                                <span class="meta-date">
                                                                                    <time datetime="2015-04-03T15:04:50+00:00">
                                                                                        <i class="fa fa-clock-o"></i>March 14, 2015
                                                                                    </time>
                                                                                </span>
                                                                                <span class="meta-category">
                                                                                    <i class="fa fa-folder-open-o"></i>
                                                                                    <a href="#">Fashion</a>, <a href="#">Trends</a>
                                                                                </span>
                                                                                <span class="meta-comment">
                                                                                    <i class="fa fa-comment-o"></i>
                                                                                    <a href="#" class="meta-comments">0 Comment</a>
                                                                                </span> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="entry-content">
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec fringilla diam nunc, a porta massa commodo a. Maecenas tincidunt lacinia...
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row section-contact-info">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="raw-html">
                                                        <div class="footer-contact-icon">
                                                            <a href="mailto:info@yolia.com">
                                                                <i class="el-appear elegant_icon_mail_alt animate-appear"></i>
                                                                <span class="footer-contact-icon-link">info@yolia.com</span>
                                                                <span class="footer-contact-icon-text">Email</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="raw-html">
                                                        <div class="footer-contact-icon">
                                                            <a href="#">
                                                                <i class="el-appear elegant_icon_pin_alt animate-appear"></i>
                                                                <span class="footer-contact-icon-link">SUITE 1600, DALLAS, TX 725A1</span>
                                                                <span class="footer-contact-icon-text">VISIT</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="raw-html">
                                                        <div class="footer-contact-icon">
                                                            <a href="javascript:void(0)">
                                                                <i class="el-appear elegant_icon_calulator animate-appear"></i>
                                                                <span class="footer-contact-icon-link">+01 2 3456 7891</span>
                                                                <span class="footer-contact-icon-text">PHONE</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-image-bg row-image-bg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php include('footer.php'); ?>