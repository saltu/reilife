<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">

            <?php include('header.php');?>
            
            <div class="content-container" style="padding-top: 98px !important;">
                <div class="heading-container heading-about ">
                    <div class="heading-standar">
                        <div class="heading-wrap">
                            <div class="container">
                                <div class="page-title">
                                    <h1>About Us</h1>
                                </div>
                            </div>
                            <div class="page-breadcrumb">
                                <div class="container">
                                    <ul class="breadcrumb">
                                        <li><a class="home" href="#"><span>Home</span></a></li>
                                        <li>About Us</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="section-about">
                                    <div class="container">
                                        <div class="row">
                                            <div class="column col-md-6 col-sm-6">
                                                <div class="single-image image-right">
                                                    <img width="554" height="378" src="images/about.jpg" alt="about"/>
                                                </div>
                                            </div>
                                            <div class="column col-md-6 col-sm-6">
                                                <h2>About us</h2>
                                                <div class="text-block">
                                                    <p>
                                                        Rei Life holistic healing  foundation is an Institute Promoting holistic healing and Alternative Therapies Like Reiki,Acupressure/Reflexology, EFT, NLP, Bach Flower remedies etc.

                                                    </p>
                                                    <blockquote>
                                                        <p>
                                                            If you are looking for a change, emotional support, personal growth, empowerment and some tools to start living your Greater potential, then you will certainly find something here to help.
                                                        </p>
                                                    </blockquote>
                                                    <p>So, what does <b> "Holistic"  </b>mean ?</p>
                                                    <p>Originating from the Greek word “Holos”, meaning “whole” or “complete”, holistic therapies approach disorders, ailments and disease as parts of a greater whole. So  The word "Holistic" means that the three aspects of the self, mind, body and spirit are are interconnected. An imbalance in one often leads to imbalance in the other two and if not healed in time, manifests in the form of disease. The ReiLife  is based on this philosophy and offers various Holistic therapies, classes and workshops to provide loving support and guidance on an emotional, intellectual, physical and spiritual level. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <!--  <div class="row section-services section-background-bottom">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2>OUR SERVICES</h2>
                                                    <div class="text-block">
                                                        <p>
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed nisi sem.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row section-iconbox">
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="iconbox iconbox-pos-top iconbox-bg iconbox-effect-4">
                                                        <div class="iconbox-icon icon-circle icon-color-white icon-size-lg">
                                                            <i data-toggle="iconbox" class="el-appear fa fa-weibo animate-appear"></i>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h3 class="el-heading">BEAUTY</h3>
                                                            <p>
                                                                Ut ultricies quis nulla non lacinia. Suspendisse posuere orci enim, ac imperdiet diam mattis nec. Etiam ante nisi.
                                                            </p>
                                                            <a class="text-white" href="#">
                                                                See More 
                                                                <i class="fa fa-arrow-circle-o-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="iconbox iconbox-pos-top iconbox-bg  iconbox-effect-4">
                                                        <div class="iconbox-icon icon-circle icon-color-white icon-size-lg">
                                                            <i data-toggle="iconbox" class="el-appear fa fa-tencent-weibo animate-appear"></i>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h3 class="el-heading">SPA</h3>
                                                            <p>
                                                                Phasellus ac vulputate leo. Fusce non magna consequat, feugiat ex eu, dapibus ligula. Sed non faucibus risus. In hac habitasse platea dictumst.
                                                            </p>
                                                            <a class="text-white" href="#">
                                                                See More 
                                                                <i class="fa fa-arrow-circle-o-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="iconbox iconbox-pos-top iconbox-bg iconbox-effect-4">
                                                        <div class="iconbox-icon icon-circle icon-color-white icon-size-lg">
                                                            <i data-toggle="iconbox" class="el-appear fa fa-reddit animate-appear"></i>
                                                        </div>
                                                        <div class="iconbox-content">
                                                            <h3 class="el-heading">MESSAGE</h3>
                                                            <p>
                                                                Cras sollicitudin nec est sit amet egestas. Proin sed nisl quis tellus pulvinar vestibulum. In elementum lectus quis velit maximus elementum.
                                                            </p>
                                                            <a class="text-white" href="#">
                                                                See More 
                                                                <i class="fa fa-arrow-circle-o-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row section-counter">
                                                <div class="column col-md-3 col-sm-6">
                                                    <div class="counter counter-icon-left">
                                                        <span class="el-appear counter-icon animate-appear">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                        <div class="counter-count">
                                                            <span class="counter-number" data-to="17483" data-speed="2000">
                                                                17483
                                                            </span>
                                                            <div class="counter-text">Happy Customer</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-3 col-sm-6">
                                                    <div class="counter counter-icon-left">
                                                        <span class="el-appear counter-icon animate-appear">
                                                            <i class="fa fa-rocket"></i>
                                                        </span>
                                                        <div class="counter-count">
                                                            <span class="counter-number" data-to="465" data-speed="2000">
                                                                465
                                                            </span>
                                                            <div class="counter-text">Beauty Styles</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-3 col-sm-6">
                                                    <div class="counter counter-icon-left">
                                                        <span class="el-appear counter-icon animate-appear">
                                                            <i class="fa fa-pagelines"></i>
                                                        </span>
                                                        <div class="counter-count">
                                                            <span class="counter-number" data-to="96" data-speed="2000">
                                                                96
                                                            </span>
                                                            <div class="counter-text">Procedures</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-3 col-sm-6">
                                                    <div class="counter counter-icon-left">
                                                        <span class="el-appear counter-icon animate-appear">
                                                            <i class="fa fa-heart"></i>
                                                        </span>
                                                        <div class="counter-count">
                                                            <span class="counter-number" data-to="345" data-speed="2000">
                                                                345
                                                            </span>
                                                            <div class="counter-text">Treatments</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="row section-team">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h2>OUR TEAM</h2>
                                                    <div class="text-block">
                                                        <p>
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed nisi sem.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row team-members">
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="team-member team-member-center_bottom">
                                                        <div class="member-avatar">
                                                            <img src="images/team/team-member-1.jpg" width="370" height="410" alt="team-member-1" />
                                                        </div>
                                                        <div class="member-info">
                                                            <div class="member-info-wrap">
                                                                <div class="member-name">
                                                                    <h4>Duis Lectus</h4>
                                                                </div>
                                                                <div class="member-job">Developer</div>
                                                                <div class="member-info-line"></div>
                                                                <div class="member-meta">
                                                                    <span class="facebook">
                                                                        <a href="#" title="Facebook" target="_blank">
                                                                            <i class="fa fa-facebook"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="twitter">
                                                                        <a href="#" title="Twitter" target="_blank">
                                                                            <i class="fa fa-twitter"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="google">
                                                                        <a href="#" title="Google +" target="_blank">
                                                                            <i class="fa fa-google"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="linkedin">
                                                                        <a href="#" title="Linked In" target="_blank">
                                                                            <i class="fa fa-linkedin"></i>
                                                                        </a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="team-member team-member-center_top">
                                                        <div class="member-info">
                                                            <div class="member-info-wrap">
                                                                <div class="member-name">
                                                                    <h4>Fusce dignissim</h4>
                                                                </div>
                                                                <div class="member-job">Designer</div>
                                                                <div class="member-info-line"></div>
                                                                <div class="member-meta">
                                                                    <span class="facebook">
                                                                        <a href="#" title="Facebook" target="_blank">
                                                                            <i class="fa fa-facebook"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="twitter">
                                                                        <a href="#" title="Twitter" target="_blank">
                                                                            <i class="fa fa-twitter"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="google">
                                                                        <a href="#" title="Google +" target="_blank">
                                                                            <i class="fa fa-google"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="linkedin">
                                                                        <a href="#" title="Linked In" target="_blank">
                                                                            <i class="fa fa-linkedin"></i>
                                                                        </a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="member-avatar">
                                                            <img src="images/team/team-member-2.jpg" width="370" height="410" alt="team-member-2" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="team-member team-member-center_bottom">
                                                        <div class="member-avatar">
                                                            <img src="images/team/team-member-3.jpg" width="370" height="410" alt="team-member-3" />
                                                        </div>
                                                        <div class="member-info">
                                                            <div class="member-info-wrap">
                                                                <div class="member-name">
                                                                    <h4>Phasellus por</h4>
                                                                </div>
                                                                <div class="member-job">UX Designer</div>
                                                                <div class="member-info-line"></div>
                                                                <div class="member-meta">
                                                                    <span class="facebook">
                                                                        <a href="#" title="Facebook" target="_blank">
                                                                            <i class="fa fa-facebook"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="twitter">
                                                                        <a href="#" title="Twitter" target="_blank">
                                                                            <i class="fa fa-twitter"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="google">
                                                                        <a href="#" title="Google +" target="_blank">
                                                                            <i class="fa fa-google"></i>
                                                                        </a>
                                                                    </span>
                                                                    <span class="linkedin">
                                                                        <a href="#" title="Linked In" target="_blank">
                                                                            <i class="fa fa-linkedin"></i>
                                                                        </a>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row section-newsletters">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-12">
                                                    <h3 class="text-center">
                                                        STAY INFORMED WITH OUR NEWSLETTER
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row form-newsletters">
                                                <div class="column col-md-3 col-sm-6"></div>
                                                <div class="column col-md-6 col-sm-6">
                                                    <div class="widget">
                                                        <form>
                                                            <div class="clearfix">
                                                                <div class="row">
                                                                    <div class="col-sm-8">
                                                                        <label class="hide">Subscribe</label>
                                                                        <input type="email" class="form-control" required="required" placeholder="Enter your email..." name="email" />
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <button type="submit" class="btn btn-primary">
                                                                            Subscribe
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="column col-md-3 col-sm-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php include('footer.php'); ?>

        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>
        
        <script type='text/javascript' src='js/libs/jquery.js'></script>
        <script type='text/javascript' src='js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='js/libs/easing.min.js'></script>
        <script type='text/javascript' src='js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.parallax.js'></script>

        <script type='text/javascript' src='js/skin-selector.js'></script>

        <script type='text/javascript' src='js/script.js'></script>

    </body>
</html>