<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
            <?php include('header.php');?>
        <div class="content-container" style="padding-top: 98px !important; padding-bottom: 0px !important;">
                <div class="heading-container heading-contact">
                    <div class="heading-standar">
                        <div class="heading-wrap">
                            <div class="container">
                                <div class="page-title">
                                    <h1>Contact Us</h1>
                                </div>
                            </div>
                            <div class="page-breadcrumb">
                                <div class="container">
                                    <ul class="breadcrumb">
                                        <li><a class="home" href="#"><span>Home</span></a></li>
                                        <li>Contact Us</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="content-container no-padding">
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="row section-contact">
                                    <div class="column col-md-12">
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-8 col-sm-8">
                                                    <h3>SEND US A MESSAGE</h3>
                                                    <div class="empty-space-40"></div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <h3>MAIN OFFICE</h3>
                                                    <div class="empty-space-40"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="column col-md-8 col-sm-8">
                                                    <form>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <p>
                                                                    Your Name (required)<br/>
                                                                    <span class="form-control-wrap">
                                                                        <input type="text" name="your-name" size="40" class="form-control" />
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    Your Email (required)<br/>
                                                                    <span class="form-control-wrap">
                                                                        <input type="email" name="your-email" size="40" class="form-control" />
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <p>
                                                                    Subject<br/>
                                                                    <span class="form-control-wrap">
                                                                        <input type="text" name="your-subject" size="40" class="form-control" />
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>
                                                                    Your Message<br/>
                                                                    <span class="form-control-wrap">
                                                                        <textarea name="your-message" cols="40" rows="6" class="form-control textarea"></textarea>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <p>
                                                                    <input type="submit" value="Send" class="form-control submit"/>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="column col-md-4 col-sm-4">
                                                    <div class="text-block">
                                                        <p><strong>Address:</strong></p>
                                                        <p>A1, 1st Floor<br>
                                                        Kaustubham, Nandnath Kochako Rd<br>
                                                        Chakaraparambu<br>
                                                        Ernakulam- 32
                                                        .</p>
                                                        <p><strong>Email: </strong></p>
                                                        <p><a href="mailto:info@reilife.com">info@reilife.com</a></p>
                                                        <p><strong>Phone: </strong></p>
                                                        <p> +91 9847494961,<br> +91 9747910823</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="column col-md-12">
                                        <div data-toggle="gmap" class="google-map">
                                            <div class="gmap" data-greyscale="0" data-markers="[{&quot;latitude&quot;:40.7119108189,&quot;longitude&quot;:-74.0039920807,&quot;info&quot;:&quot;Come visit us at our location!&quot;},{&quot;latitude&quot;:40.7113252804,&quot;longitude&quot;:-74.0109443665,&quot;info&quot;:&quot;Come visit us at our location 2 !&quot;}]" data-enable-zoom="1" data-zoom="15" data-center-latitude="40.71260803688496" data-center-longitude="-74.00566577911377" data-marker-icon="images/marker.png"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer id="footer" class="footer">
                <div class="footer-info">
                    <div class="container">
                        <div class="footer-info-wrap">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="copyright">Copyright © 2019 Relife. All Rights Reserved.</div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="footer-info-text text-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>

        <script type='text/javascript' src='js/libs/jquery.js'></script>
        <script type='text/javascript' src='js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='js/libs/easing.min.js'></script>
        <script type='text/javascript' src='js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.parallax.js'></script>

        <script type='text/javascript' src='js/skin-selector.js'></script>

        <script type='text/javascript' src='js/script.js'></script>
    </body>
</html>