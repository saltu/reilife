<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Home | Reilife</title>
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel='stylesheet' href='css/settings.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Muli' type='text/css' media='all'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Montserrat' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/elegant-icon.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/section.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/shop.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/preloader.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/magnific-popup.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/form-style.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/skin-selector.css' type='text/css' media='all'/>
    </head>
    
    <body data-spy="scroll">
        <div id="preloader">
            <img class="preloader__logo" src="images/logo.png" alt=""/>
            <div class="preloader__progress">
                <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
                    <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4" stroke-linecap="round" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                    <path id='preloader__progress-circle' fill="none" stroke="#7ccedf" stroke-width="4" stroke-linecap="round" stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                </svg>
            </div>
        </div>
        <div id="wrapper" class="wide-wrap">
            <?php include('header.php');?>    
            <div class="content-container" style="padding-top: 98px !important;">
                <div class="heading-container">
                    <div class="heading-standar">
                        <div class="heading-wrap">
                            <div class="container">
                                <div class="page-title">
                                    <h1>Events</h1>
                                </div>
                            </div>
                            <div class="page-breadcrumb">
                                <div class="container">
                                    <ul class="breadcrumb">
                                        <li><a class="home" href="#"><span>Home</span></a></li>
                                        <li>Events</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-container">
                <div class="container-full">
                    <div class="row">
                        <div class="col-md-12 main-wrap">
                            <div class="main-content">
                                <div class="portfolio portfolio-style-one masonry gap" data-paginate="no" data-layout="masonry" data-masonry-column="3">
                                    <div class="container">
                                        <div class="portfolio-filter">
                                            <div class="filter-heaeding">
                                                <h3>All</h3>
                                            </div>
                                            <div class="filter-action filter-action-default no-sorting">
                                                <ul data-filter-key="filter">
                                                    <li><a class="selected" href="#" data-filter-value="*">All</a></li>
                                                    <li><a href="#" data-filter-value=".donec">Donec</a></li>
                                                    <li><a href="#" data-filter-value=".nullam">Nullam</a></li>
                                                    <li><a href="#" data-filter-value=".quisque">Quisque</a></li>
                                                    <li><a href="#" data-filter-value=".vestibulum">Vestibulum</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="masonry-wrap portfolio-layout-masonry row">
                                        <article data-title="Gallery" data-accent-color="#bdce40" class="portfolio-item masonry-item col-md-4 col-sm-6 quisque">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured gallery-featured">
                                                        <div class="caroufredsel" data-visible="1" data-responsive="1" data-infinite="1" data-autoplay="0" data-height="variable">
                                                            <div class="caroufredsel-wrap">
                                                                <ul class="caroufredsel-items">
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b4.jpg" title="b4" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b4-thumb.jpg" alt="b4"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b12.jpg" title="b12" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b12-thumb.jpg" alt="b12"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b3.jpg" title="b3" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b3-thumb.jpg" alt="b3"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b1.jpg" title="b1" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b1-thumb.jpg" alt="b1"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b2.jpg" title="b2" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b2-thumb.jpg" alt="b2"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b5.jpg" title="b5" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b5-thumb.jpg" alt="b5"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b9.jpg" title="b9" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b9-thumb.jpg" alt="b9"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b6.jpg" title="b6" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b6-thumb.jpg" alt="b6"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b8.jpg" title="b8" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b8-thumb.jpg" alt="b8"/>
                                                                        </a>
                                                                    </li>
                                                                    <li class="caroufredsel-item">
                                                                        <a href="images/gallery/b7.jpg" title="b7" data-rel="magnific-popup">
                                                                            <img width="500" height="400" src="images/gallery/b7-thumb.jpg" alt="b7"/>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <a href="#" class="caroufredsel-prev"></a>
                                                                <a href="#" class="caroufredsel-next"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="#">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Gallery">
                                                                Gallery
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 17, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Vimeo" data-accent-color="#ed21c0" class="portfolio-item masonry-item col-md-4 col-sm-6 donec quisque">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured video-featured">
                                                        <div id="video-2" class="embed-wrap mfp-hide">
                                                            <iframe src="https://player.vimeo.com/video/40207909" width="1200" height="675" title="Villa Salika 450, Chalong Bay, Phuket." allowfullscreen></iframe>
                                                        </div>
                                                        <div class="video-poster">
                                                            <img alt="Vimeo" src="images/gallery/bvimeo-thumb.jpg" />
                                                        </div>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="#video-2" data-rel="magnific-portfolio-video">
                                                            <i class="fa fa-play"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Vimeo">Vimeo</a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 17, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Youtube" data-accent-color="#7eb738" class="portfolio-item masonry-item col-md-4 col-sm-6 quisque vestibulum">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured video-featured">
                                                        <div id="video-1" class="embed-wrap mfp-hide">
                                                            <iframe src="https://player.vimeo.com/video/40207909" width="1200" height="675" title="Villa Salika 450, Chalong Bay, Phuket." allowfullscreen></iframe>
                                                        </div>
                                                        <div class="video-poster">
                                                            <img alt="Youtube" src="images/gallery/byoutube-thumb.jpg" />
                                                        </div>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="#video-1" data-rel="magnific-portfolio-video">
                                                            <i class="fa fa-play"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Youtube">Youtube</a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 17, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Purus sodales tincidunt" data-accent-color="#81d742" class="portfolio-item masonry-item col-md-4 col-sm-6 nullam quisque">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b5-thumb.jpg" alt="b5"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b5.jpg" title="b5" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Purus sodales tincidunt">
                                                                Purus sodales tincidunt
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Curabitur at" data-accent-color="#dd9933" class="portfolio-item masonry-item col-md-4 col-sm-6 donec nullam">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b10-thumb.jpg"  alt="b10"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b10.jpg" title="b10" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Curabitur at">
                                                                Curabitur at
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Nulla facilisi" data-accent-color="#33a555" class="portfolio-item masonry-item col-md-4 col-sm-6 donec">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b11-thumb.jpg"  alt="b11"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b11.jpg" title="b9" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                Nulla facilisi
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Ultricies nunc dapibus" data-accent-color="#dd3333" class="portfolio-item masonry-item col-md-4 col-sm-6 nullam">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b8-thumb.jpg"  alt="b8"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b8.jpg" title="b8" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                Ultricies nunc dapibus
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Rhoncus lectus" data-accent-color="#e8c020" class="portfolio-item masonry-item col-md-4 col-sm-6 donec nullam">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b6-thumb.jpg"  alt="b6"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b6.jpg" title="b6" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                Rhoncus lectus
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Sed dignissim" data-accent-color="#e85935" class="portfolio-item masonry-item col-md-4 col-sm-6 quisque">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b13-thumb.jpg"  alt="b13"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b13.jpg" title="b13" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                Sed dignissim
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="In rhoncus lectus" data-accent-color="#2f932d" class="portfolio-item masonry-item col-md-4 col-sm-6 quisque">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b1-thumb.jpg"  alt="b1"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b1.jpg" title="b1" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                In rhoncus lectus
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Donec congue euismod elit" data-accent-color="#89a32a" class="portfolio-item masonry-item col-md-4 col-sm-6 nullam">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b14-thumb.jpg"  alt="b14"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b14.jpg" title="b14" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                Donec congue euismod elit
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <article data-title="Quisque vitae tellus" data-accent-color="#b7752a" class="portfolio-item masonry-item col-md-4 col-sm-6 donec">
                                            <div class="portfolio-item-wrap">
                                                <div class="portfolio-featured-wrap">
                                                    <div class="portfolio-featured">
                                                        <a href="#">
                                                            <img width="500" height="400" src="images/gallery/b9-thumb.jpg" alt="b9"/>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-action">
                                                        <a class="zoom-action" href="images/gallery/b9.jpg" title="b9" data-rel="magnific-single-popup">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a class="view-action" href="#">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-overlay"></div>
                                                </div>
                                                <div class="portfolio-caption">
                                                    <div class="portfolio-caption-wrap">
                                                        <h2 class="portfolio-title">
                                                            <a href="#" title="Nulla facilisi">
                                                                Quisque vitae tellus
                                                            </a>
                                                        </h2>
                                                        <div class="portfolio-meta">
                                                            May 16, 2015
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include('footer.php'); ?>

        <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>

        <script type='text/javascript' src='js/libs/jquery.js'></script>
        <script type='text/javascript' src='js/libs/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.tools.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.themepunch.revolution.min.js'></script>
        <script type='text/javascript' src='js/libs/preloader.min.js'></script>

        <script type='text/javascript' src='js/libs/easing.min.js'></script>
        <script type='text/javascript' src='js/libs/imagesloaded.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/bootstrap.min.js'></script>
        <script type='text/javascript' src='js/libs/superfish-1.7.4.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.appear.min.js'></script>
        
        <script type='text/javascript' src='js/libs/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.carouFredSel.min.js'></script>
        <script type='text/javascript' src='js/libs/isotope.pkgd.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.magnific-popup.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.countTo.min.js'></script>
        <script type='text/javascript' src='js/libs/jquery.parallax.js'></script>

        <script type='text/javascript' src='js/skin-selector.js'></script>

        <script type='text/javascript' src='js/script.js'></script>
    </body>
</html>